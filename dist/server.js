require("source-map-support").install();
/******/ (function(modules) { // webpackBootstrap
/******/ 	function hotDownloadUpdateChunk(chunkId) { // eslint-disable-line no-unused-vars
/******/ 		var chunk = require("./" + "" + chunkId + "." + hotCurrentHash + ".hot-update.js");
/******/ 		hotAddUpdateChunk(chunk.id, chunk.modules);
/******/ 	}
/******/ 	
/******/ 	function hotDownloadManifest() { // eslint-disable-line no-unused-vars
/******/ 		try {
/******/ 			var update = require("./" + "" + hotCurrentHash + ".hot-update.json");
/******/ 		} catch(e) {
/******/ 			return Promise.resolve();
/******/ 		}
/******/ 		return Promise.resolve(update);
/******/ 	}
/******/ 	
/******/ 	function hotDisposeChunk(chunkId) { //eslint-disable-line no-unused-vars
/******/ 		delete installedChunks[chunkId];
/******/ 	}
/******/
/******/ 	
/******/ 	
/******/ 	var hotApplyOnUpdate = true;
/******/ 	var hotCurrentHash = "35ee94c5f387f97f5b3e"; // eslint-disable-line no-unused-vars
/******/ 	var hotRequestTimeout = 10000;
/******/ 	var hotCurrentModuleData = {};
/******/ 	var hotCurrentChildModule; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParents = []; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParentsTemp = []; // eslint-disable-line no-unused-vars
/******/ 	
/******/ 	function hotCreateRequire(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var me = installedModules[moduleId];
/******/ 		if(!me) return __webpack_require__;
/******/ 		var fn = function(request) {
/******/ 			if(me.hot.active) {
/******/ 				if(installedModules[request]) {
/******/ 					if(installedModules[request].parents.indexOf(moduleId) < 0)
/******/ 						installedModules[request].parents.push(moduleId);
/******/ 				} else {
/******/ 					hotCurrentParents = [moduleId];
/******/ 					hotCurrentChildModule = request;
/******/ 				}
/******/ 				if(me.children.indexOf(request) < 0)
/******/ 					me.children.push(request);
/******/ 			} else {
/******/ 				console.warn("[HMR] unexpected require(" + request + ") from disposed module " + moduleId);
/******/ 				hotCurrentParents = [];
/******/ 			}
/******/ 			return __webpack_require__(request);
/******/ 		};
/******/ 		var ObjectFactory = function ObjectFactory(name) {
/******/ 			return {
/******/ 				configurable: true,
/******/ 				enumerable: true,
/******/ 				get: function() {
/******/ 					return __webpack_require__[name];
/******/ 				},
/******/ 				set: function(value) {
/******/ 					__webpack_require__[name] = value;
/******/ 				}
/******/ 			};
/******/ 		};
/******/ 		for(var name in __webpack_require__) {
/******/ 			if(Object.prototype.hasOwnProperty.call(__webpack_require__, name) && name !== "e") {
/******/ 				Object.defineProperty(fn, name, ObjectFactory(name));
/******/ 			}
/******/ 		}
/******/ 		fn.e = function(chunkId) {
/******/ 			if(hotStatus === "ready")
/******/ 				hotSetStatus("prepare");
/******/ 			hotChunksLoading++;
/******/ 			return __webpack_require__.e(chunkId).then(finishChunkLoading, function(err) {
/******/ 				finishChunkLoading();
/******/ 				throw err;
/******/ 			});
/******/ 	
/******/ 			function finishChunkLoading() {
/******/ 				hotChunksLoading--;
/******/ 				if(hotStatus === "prepare") {
/******/ 					if(!hotWaitingFilesMap[chunkId]) {
/******/ 						hotEnsureUpdateChunk(chunkId);
/******/ 					}
/******/ 					if(hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 						hotUpdateDownloaded();
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 		return fn;
/******/ 	}
/******/ 	
/******/ 	function hotCreateModule(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var hot = {
/******/ 			// private stuff
/******/ 			_acceptedDependencies: {},
/******/ 			_declinedDependencies: {},
/******/ 			_selfAccepted: false,
/******/ 			_selfDeclined: false,
/******/ 			_disposeHandlers: [],
/******/ 			_main: hotCurrentChildModule !== moduleId,
/******/ 	
/******/ 			// Module API
/******/ 			active: true,
/******/ 			accept: function(dep, callback) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfAccepted = true;
/******/ 				else if(typeof dep === "function")
/******/ 					hot._selfAccepted = dep;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._acceptedDependencies[dep[i]] = callback || function() {};
/******/ 				else
/******/ 					hot._acceptedDependencies[dep] = callback || function() {};
/******/ 			},
/******/ 			decline: function(dep) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfDeclined = true;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._declinedDependencies[dep[i]] = true;
/******/ 				else
/******/ 					hot._declinedDependencies[dep] = true;
/******/ 			},
/******/ 			dispose: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			addDisposeHandler: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			removeDisposeHandler: function(callback) {
/******/ 				var idx = hot._disposeHandlers.indexOf(callback);
/******/ 				if(idx >= 0) hot._disposeHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			// Management API
/******/ 			check: hotCheck,
/******/ 			apply: hotApply,
/******/ 			status: function(l) {
/******/ 				if(!l) return hotStatus;
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			addStatusHandler: function(l) {
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			removeStatusHandler: function(l) {
/******/ 				var idx = hotStatusHandlers.indexOf(l);
/******/ 				if(idx >= 0) hotStatusHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			//inherit from previous dispose call
/******/ 			data: hotCurrentModuleData[moduleId]
/******/ 		};
/******/ 		hotCurrentChildModule = undefined;
/******/ 		return hot;
/******/ 	}
/******/ 	
/******/ 	var hotStatusHandlers = [];
/******/ 	var hotStatus = "idle";
/******/ 	
/******/ 	function hotSetStatus(newStatus) {
/******/ 		hotStatus = newStatus;
/******/ 		for(var i = 0; i < hotStatusHandlers.length; i++)
/******/ 			hotStatusHandlers[i].call(null, newStatus);
/******/ 	}
/******/ 	
/******/ 	// while downloading
/******/ 	var hotWaitingFiles = 0;
/******/ 	var hotChunksLoading = 0;
/******/ 	var hotWaitingFilesMap = {};
/******/ 	var hotRequestedFilesMap = {};
/******/ 	var hotAvailableFilesMap = {};
/******/ 	var hotDeferred;
/******/ 	
/******/ 	// The update info
/******/ 	var hotUpdate, hotUpdateNewHash;
/******/ 	
/******/ 	function toModuleId(id) {
/******/ 		var isNumber = (+id) + "" === id;
/******/ 		return isNumber ? +id : id;
/******/ 	}
/******/ 	
/******/ 	function hotCheck(apply) {
/******/ 		if(hotStatus !== "idle") throw new Error("check() is only allowed in idle status");
/******/ 		hotApplyOnUpdate = apply;
/******/ 		hotSetStatus("check");
/******/ 		return hotDownloadManifest(hotRequestTimeout).then(function(update) {
/******/ 			if(!update) {
/******/ 				hotSetStatus("idle");
/******/ 				return null;
/******/ 			}
/******/ 			hotRequestedFilesMap = {};
/******/ 			hotWaitingFilesMap = {};
/******/ 			hotAvailableFilesMap = update.c;
/******/ 			hotUpdateNewHash = update.h;
/******/ 	
/******/ 			hotSetStatus("prepare");
/******/ 			var promise = new Promise(function(resolve, reject) {
/******/ 				hotDeferred = {
/******/ 					resolve: resolve,
/******/ 					reject: reject
/******/ 				};
/******/ 			});
/******/ 			hotUpdate = {};
/******/ 			var chunkId = 0;
/******/ 			{ // eslint-disable-line no-lone-blocks
/******/ 				/*globals chunkId */
/******/ 				hotEnsureUpdateChunk(chunkId);
/******/ 			}
/******/ 			if(hotStatus === "prepare" && hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 				hotUpdateDownloaded();
/******/ 			}
/******/ 			return promise;
/******/ 		});
/******/ 	}
/******/ 	
/******/ 	function hotAddUpdateChunk(chunkId, moreModules) { // eslint-disable-line no-unused-vars
/******/ 		if(!hotAvailableFilesMap[chunkId] || !hotRequestedFilesMap[chunkId])
/******/ 			return;
/******/ 		hotRequestedFilesMap[chunkId] = false;
/******/ 		for(var moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				hotUpdate[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(--hotWaitingFiles === 0 && hotChunksLoading === 0) {
/******/ 			hotUpdateDownloaded();
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotEnsureUpdateChunk(chunkId) {
/******/ 		if(!hotAvailableFilesMap[chunkId]) {
/******/ 			hotWaitingFilesMap[chunkId] = true;
/******/ 		} else {
/******/ 			hotRequestedFilesMap[chunkId] = true;
/******/ 			hotWaitingFiles++;
/******/ 			hotDownloadUpdateChunk(chunkId);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotUpdateDownloaded() {
/******/ 		hotSetStatus("ready");
/******/ 		var deferred = hotDeferred;
/******/ 		hotDeferred = null;
/******/ 		if(!deferred) return;
/******/ 		if(hotApplyOnUpdate) {
/******/ 			// Wrap deferred object in Promise to mark it as a well-handled Promise to
/******/ 			// avoid triggering uncaught exception warning in Chrome.
/******/ 			// See https://bugs.chromium.org/p/chromium/issues/detail?id=465666
/******/ 			Promise.resolve().then(function() {
/******/ 				return hotApply(hotApplyOnUpdate);
/******/ 			}).then(
/******/ 				function(result) {
/******/ 					deferred.resolve(result);
/******/ 				},
/******/ 				function(err) {
/******/ 					deferred.reject(err);
/******/ 				}
/******/ 			);
/******/ 		} else {
/******/ 			var outdatedModules = [];
/******/ 			for(var id in hotUpdate) {
/******/ 				if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 					outdatedModules.push(toModuleId(id));
/******/ 				}
/******/ 			}
/******/ 			deferred.resolve(outdatedModules);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotApply(options) {
/******/ 		if(hotStatus !== "ready") throw new Error("apply() is only allowed in ready status");
/******/ 		options = options || {};
/******/ 	
/******/ 		var cb;
/******/ 		var i;
/******/ 		var j;
/******/ 		var module;
/******/ 		var moduleId;
/******/ 	
/******/ 		function getAffectedStuff(updateModuleId) {
/******/ 			var outdatedModules = [updateModuleId];
/******/ 			var outdatedDependencies = {};
/******/ 	
/******/ 			var queue = outdatedModules.slice().map(function(id) {
/******/ 				return {
/******/ 					chain: [id],
/******/ 					id: id
/******/ 				};
/******/ 			});
/******/ 			while(queue.length > 0) {
/******/ 				var queueItem = queue.pop();
/******/ 				var moduleId = queueItem.id;
/******/ 				var chain = queueItem.chain;
/******/ 				module = installedModules[moduleId];
/******/ 				if(!module || module.hot._selfAccepted)
/******/ 					continue;
/******/ 				if(module.hot._selfDeclined) {
/******/ 					return {
/******/ 						type: "self-declined",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				if(module.hot._main) {
/******/ 					return {
/******/ 						type: "unaccepted",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				for(var i = 0; i < module.parents.length; i++) {
/******/ 					var parentId = module.parents[i];
/******/ 					var parent = installedModules[parentId];
/******/ 					if(!parent) continue;
/******/ 					if(parent.hot._declinedDependencies[moduleId]) {
/******/ 						return {
/******/ 							type: "declined",
/******/ 							chain: chain.concat([parentId]),
/******/ 							moduleId: moduleId,
/******/ 							parentId: parentId
/******/ 						};
/******/ 					}
/******/ 					if(outdatedModules.indexOf(parentId) >= 0) continue;
/******/ 					if(parent.hot._acceptedDependencies[moduleId]) {
/******/ 						if(!outdatedDependencies[parentId])
/******/ 							outdatedDependencies[parentId] = [];
/******/ 						addAllToSet(outdatedDependencies[parentId], [moduleId]);
/******/ 						continue;
/******/ 					}
/******/ 					delete outdatedDependencies[parentId];
/******/ 					outdatedModules.push(parentId);
/******/ 					queue.push({
/******/ 						chain: chain.concat([parentId]),
/******/ 						id: parentId
/******/ 					});
/******/ 				}
/******/ 			}
/******/ 	
/******/ 			return {
/******/ 				type: "accepted",
/******/ 				moduleId: updateModuleId,
/******/ 				outdatedModules: outdatedModules,
/******/ 				outdatedDependencies: outdatedDependencies
/******/ 			};
/******/ 		}
/******/ 	
/******/ 		function addAllToSet(a, b) {
/******/ 			for(var i = 0; i < b.length; i++) {
/******/ 				var item = b[i];
/******/ 				if(a.indexOf(item) < 0)
/******/ 					a.push(item);
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// at begin all updates modules are outdated
/******/ 		// the "outdated" status can propagate to parents if they don't accept the children
/******/ 		var outdatedDependencies = {};
/******/ 		var outdatedModules = [];
/******/ 		var appliedUpdate = {};
/******/ 	
/******/ 		var warnUnexpectedRequire = function warnUnexpectedRequire() {
/******/ 			console.warn("[HMR] unexpected require(" + result.moduleId + ") to disposed module");
/******/ 		};
/******/ 	
/******/ 		for(var id in hotUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 				moduleId = toModuleId(id);
/******/ 				var result;
/******/ 				if(hotUpdate[id]) {
/******/ 					result = getAffectedStuff(moduleId);
/******/ 				} else {
/******/ 					result = {
/******/ 						type: "disposed",
/******/ 						moduleId: id
/******/ 					};
/******/ 				}
/******/ 				var abortError = false;
/******/ 				var doApply = false;
/******/ 				var doDispose = false;
/******/ 				var chainInfo = "";
/******/ 				if(result.chain) {
/******/ 					chainInfo = "\nUpdate propagation: " + result.chain.join(" -> ");
/******/ 				}
/******/ 				switch(result.type) {
/******/ 					case "self-declined":
/******/ 						if(options.onDeclined)
/******/ 							options.onDeclined(result);
/******/ 						if(!options.ignoreDeclined)
/******/ 							abortError = new Error("Aborted because of self decline: " + result.moduleId + chainInfo);
/******/ 						break;
/******/ 					case "declined":
/******/ 						if(options.onDeclined)
/******/ 							options.onDeclined(result);
/******/ 						if(!options.ignoreDeclined)
/******/ 							abortError = new Error("Aborted because of declined dependency: " + result.moduleId + " in " + result.parentId + chainInfo);
/******/ 						break;
/******/ 					case "unaccepted":
/******/ 						if(options.onUnaccepted)
/******/ 							options.onUnaccepted(result);
/******/ 						if(!options.ignoreUnaccepted)
/******/ 							abortError = new Error("Aborted because " + moduleId + " is not accepted" + chainInfo);
/******/ 						break;
/******/ 					case "accepted":
/******/ 						if(options.onAccepted)
/******/ 							options.onAccepted(result);
/******/ 						doApply = true;
/******/ 						break;
/******/ 					case "disposed":
/******/ 						if(options.onDisposed)
/******/ 							options.onDisposed(result);
/******/ 						doDispose = true;
/******/ 						break;
/******/ 					default:
/******/ 						throw new Error("Unexception type " + result.type);
/******/ 				}
/******/ 				if(abortError) {
/******/ 					hotSetStatus("abort");
/******/ 					return Promise.reject(abortError);
/******/ 				}
/******/ 				if(doApply) {
/******/ 					appliedUpdate[moduleId] = hotUpdate[moduleId];
/******/ 					addAllToSet(outdatedModules, result.outdatedModules);
/******/ 					for(moduleId in result.outdatedDependencies) {
/******/ 						if(Object.prototype.hasOwnProperty.call(result.outdatedDependencies, moduleId)) {
/******/ 							if(!outdatedDependencies[moduleId])
/******/ 								outdatedDependencies[moduleId] = [];
/******/ 							addAllToSet(outdatedDependencies[moduleId], result.outdatedDependencies[moduleId]);
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 				if(doDispose) {
/******/ 					addAllToSet(outdatedModules, [result.moduleId]);
/******/ 					appliedUpdate[moduleId] = warnUnexpectedRequire;
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Store self accepted outdated modules to require them later by the module system
/******/ 		var outdatedSelfAcceptedModules = [];
/******/ 		for(i = 0; i < outdatedModules.length; i++) {
/******/ 			moduleId = outdatedModules[i];
/******/ 			if(installedModules[moduleId] && installedModules[moduleId].hot._selfAccepted)
/******/ 				outdatedSelfAcceptedModules.push({
/******/ 					module: moduleId,
/******/ 					errorHandler: installedModules[moduleId].hot._selfAccepted
/******/ 				});
/******/ 		}
/******/ 	
/******/ 		// Now in "dispose" phase
/******/ 		hotSetStatus("dispose");
/******/ 		Object.keys(hotAvailableFilesMap).forEach(function(chunkId) {
/******/ 			if(hotAvailableFilesMap[chunkId] === false) {
/******/ 				hotDisposeChunk(chunkId);
/******/ 			}
/******/ 		});
/******/ 	
/******/ 		var idx;
/******/ 		var queue = outdatedModules.slice();
/******/ 		while(queue.length > 0) {
/******/ 			moduleId = queue.pop();
/******/ 			module = installedModules[moduleId];
/******/ 			if(!module) continue;
/******/ 	
/******/ 			var data = {};
/******/ 	
/******/ 			// Call dispose handlers
/******/ 			var disposeHandlers = module.hot._disposeHandlers;
/******/ 			for(j = 0; j < disposeHandlers.length; j++) {
/******/ 				cb = disposeHandlers[j];
/******/ 				cb(data);
/******/ 			}
/******/ 			hotCurrentModuleData[moduleId] = data;
/******/ 	
/******/ 			// disable module (this disables requires from this module)
/******/ 			module.hot.active = false;
/******/ 	
/******/ 			// remove module from cache
/******/ 			delete installedModules[moduleId];
/******/ 	
/******/ 			// when disposing there is no need to call dispose handler
/******/ 			delete outdatedDependencies[moduleId];
/******/ 	
/******/ 			// remove "parents" references from all children
/******/ 			for(j = 0; j < module.children.length; j++) {
/******/ 				var child = installedModules[module.children[j]];
/******/ 				if(!child) continue;
/******/ 				idx = child.parents.indexOf(moduleId);
/******/ 				if(idx >= 0) {
/******/ 					child.parents.splice(idx, 1);
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// remove outdated dependency from module children
/******/ 		var dependency;
/******/ 		var moduleOutdatedDependencies;
/******/ 		for(moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				module = installedModules[moduleId];
/******/ 				if(module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					for(j = 0; j < moduleOutdatedDependencies.length; j++) {
/******/ 						dependency = moduleOutdatedDependencies[j];
/******/ 						idx = module.children.indexOf(dependency);
/******/ 						if(idx >= 0) module.children.splice(idx, 1);
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Not in "apply" phase
/******/ 		hotSetStatus("apply");
/******/ 	
/******/ 		hotCurrentHash = hotUpdateNewHash;
/******/ 	
/******/ 		// insert new code
/******/ 		for(moduleId in appliedUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(appliedUpdate, moduleId)) {
/******/ 				modules[moduleId] = appliedUpdate[moduleId];
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// call accept handlers
/******/ 		var error = null;
/******/ 		for(moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				module = installedModules[moduleId];
/******/ 				if(module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					var callbacks = [];
/******/ 					for(i = 0; i < moduleOutdatedDependencies.length; i++) {
/******/ 						dependency = moduleOutdatedDependencies[i];
/******/ 						cb = module.hot._acceptedDependencies[dependency];
/******/ 						if(cb) {
/******/ 							if(callbacks.indexOf(cb) >= 0) continue;
/******/ 							callbacks.push(cb);
/******/ 						}
/******/ 					}
/******/ 					for(i = 0; i < callbacks.length; i++) {
/******/ 						cb = callbacks[i];
/******/ 						try {
/******/ 							cb(moduleOutdatedDependencies);
/******/ 						} catch(err) {
/******/ 							if(options.onErrored) {
/******/ 								options.onErrored({
/******/ 									type: "accept-errored",
/******/ 									moduleId: moduleId,
/******/ 									dependencyId: moduleOutdatedDependencies[i],
/******/ 									error: err
/******/ 								});
/******/ 							}
/******/ 							if(!options.ignoreErrored) {
/******/ 								if(!error)
/******/ 									error = err;
/******/ 							}
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Load self accepted modules
/******/ 		for(i = 0; i < outdatedSelfAcceptedModules.length; i++) {
/******/ 			var item = outdatedSelfAcceptedModules[i];
/******/ 			moduleId = item.module;
/******/ 			hotCurrentParents = [moduleId];
/******/ 			try {
/******/ 				__webpack_require__(moduleId);
/******/ 			} catch(err) {
/******/ 				if(typeof item.errorHandler === "function") {
/******/ 					try {
/******/ 						item.errorHandler(err);
/******/ 					} catch(err2) {
/******/ 						if(options.onErrored) {
/******/ 							options.onErrored({
/******/ 								type: "self-accept-error-handler-errored",
/******/ 								moduleId: moduleId,
/******/ 								error: err2,
/******/ 								orginalError: err, // TODO remove in webpack 4
/******/ 								originalError: err
/******/ 							});
/******/ 						}
/******/ 						if(!options.ignoreErrored) {
/******/ 							if(!error)
/******/ 								error = err2;
/******/ 						}
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				} else {
/******/ 					if(options.onErrored) {
/******/ 						options.onErrored({
/******/ 							type: "self-accept-errored",
/******/ 							moduleId: moduleId,
/******/ 							error: err
/******/ 						});
/******/ 					}
/******/ 					if(!options.ignoreErrored) {
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// handle errors in accept handlers and self accepted module load
/******/ 		if(error) {
/******/ 			hotSetStatus("fail");
/******/ 			return Promise.reject(error);
/******/ 		}
/******/ 	
/******/ 		hotSetStatus("idle");
/******/ 		return new Promise(function(resolve) {
/******/ 			resolve(outdatedModules);
/******/ 		});
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {},
/******/ 			hot: hotCreateModule(moduleId),
/******/ 			parents: (hotCurrentParentsTemp = hotCurrentParents, hotCurrentParents = [], hotCurrentParentsTemp),
/******/ 			children: []
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, hotCreateRequire(moduleId));
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// __webpack_hash__
/******/ 	__webpack_require__.h = function() { return hotCurrentHash; };
/******/
/******/ 	// Load entry module and return exports
/******/ 	return hotCreateRequire(0)(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./api/controllers.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return posts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return postTitle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return postBySlug; });

// data
var adminURL =  true ? "https://ncwr-admin.cg-dev.eu" : process.env.LOCAL_ADMIN_URL;
var baseURL = adminURL + '/wp-json/wp/v2';
var ACFendoint = adminURL + '/wp-json/acf/v3';
var postTitlesURL = adminURL + '/wp-json/myplugin/v1/post-titles';
var toJSON = function toJSON(res) {
    return res.json();
};

var posts = function posts() {
    return fetch(baseURL + '/posts/?per_page=100').then(toJSON).then(function (data) {
        return data;
    });
};

var postBySlug = function postBySlug(_, args) {
    var slug = args.slug;

    return fetch(baseURL + '/posts/?slug=' + slug).then(toJSON).then(function (data) {
        return data;
    });
};

var postTitle = function postTitle() {
    return fetch('' + postTitlesURL).then(toJSON).then(function (data) {
        return data;
    });
};


//next step resolvers

/***/ }),

/***/ "./api/graphqlRouter.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getPosts; });
/* unused harmony export getPostById */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getPostTitles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getPostBySlug; });
var _require = __webpack_require__("apollo-fetch"),
    createApolloFetch = _require.createApolloFetch;

var graphqlURL =  true ? "https://ncwr.cg-dev.eu/graphql" : process.env.LOCAL_GRAPHQL_URL;
var apolloFetch = createApolloFetch({ uri: graphqlURL }); //replace the url to reflect the online address when deploying -  don't hardcode, use variables

//get data for templates
var getPosts = function getPosts() {
  //the following query must live in resolvers.js
  //this is the query I would type in graphiql
  var query = '\n    query posts\n    {\n      posts {\n        id\n        slug\n        acf {\n          card_title\n          card_summary\n        }\n      }\n    }\n  ';
  var result = apolloFetch({ query: query }).then(function (res) {
    return res.data;
  });

  return result;
};

var getInfo = function getInfo() {
  //the following query must live in resolvers.js
  //this is the query I would type in graphiql
  var query = '\n    query posts\n    {\n      posts {\n        id\n        acf {\n           tabs {\n            heading\n            block_1\n            block_2\n          }  \n        }\n      }   \n    }\n  ';
  var result = apolloFetch({ query: query }).then(function (res) {
    return res.data;
  });

  return result;
};

// const getPostById = function (postId) {
//   //the following query must live in user.resolvers.js line 64
//   //this is the query I would type in graphiql
//   const query = `
//     query postById($postId: ID)
//       {
//         postId(id: $postId) {
//           id
//           acf {
//             tabs {
//               heading
//               block_1
//               block_2
//             }  
//           }
//         }
//       }          
//   `
//   const variables = {
//     postId: postId
//   }

//   const result = apolloFetch({ query, variables }).then(res => {
//     console.log('graphqlrouter: result:', res.data)
//     return res.data
//   })

//   return result
// }

var getPostTitles = function getPostTitles() {
  //the following query must live in resolvers.js
  //this is the query I would type in graphiql
  var query = '\n    query postTitle\n    {\n      postTitle {\n        ID\n        post_name\n        post_title\n      }\n    }\n  ';
  var result = apolloFetch({ query: query }).then(function (res) {
    return res.data;
  });

  return result;
};

var getPostBySlug = function getPostBySlug(postSlug) {
  //the following query must live in user.resolvers.js line 64
  //this is the query I would type in graphiql
  var query = '\n    query postBySlug($postSlug: String)\n      {\n        postBySlug(slug: $postSlug) {\n        slug\n        acf {\n            card_title\n            card_summary\n            fact_sheet {\n              url\n              title\n              filename\n              mime_type\n            }\n            tabs {\n              heading\n              block_1\n              block_2\n            }  \n            cards {\n              acf_fc_layout\n              ... on Intro {\n                block_1\n                block_2\n                block_3\n                level\n                duration\n                classroom\n              }\n              ... on Text {\n                type\n                text\n              }\n              ... on Likert {\n                quiz_title\n                quiz_field_name\n                quiz_thank_you_msg\n                quiz_options {\n                  quiz_input_value\n                }\n              }\n              ... on LikertGrid {\n                quiz_title\n                quiz_thank_you_msg\n                quiz_options {\n                  quiz_question\n                  quiz_field_name\n                }\n              }\n              ... on ShortText {\n                form_title\n                form_field_name\n              }\n              ... on MultipleChoice {\n                quiz_title\n                quiz_extended_text\n                quiz_field_name\n                quiz_thank_you_msg\n                quiz_options {\n                  quiz_input_value\n                  photo {\n                    ID\n                    url\n                  }\n                  quiz_prompt\n                  quiz_input_type\n                }\n              }\n              ... on MultipleChoiceMultiple {\n                quiz_description\n                quiz {\n                  quiz_form_url\n                  quiz_title\n                  quiz_extended_text\n                  quiz_field_name\n                  quiz_thank_you_message\n                  quiz_options {\n                    quiz_input_value\n                    quiz_input_type\n                  }\n                }\n              }\n              ... on MultipleChoiceGrid {\n                quiz_description\n                quiz {\n                  quiz_input_value\n                  quiz_options {\n                    quiz_title\n                    quiz_extended_text\n                    quiz_field_name\n                    quiz_tip\n                  }\n                }\n              }\n              ... on FeedBack {\n                acf_fc_layout\n                feedback_type {\n                  acf_fc_layout\n                  ... on FeedbackShorText {\n                    form_url\n                    form_field_name\n                    form_description\n                  }\n                  ... on FeedbackLikert {\n                    quiz_title\n                    quiz_options {\n                      quiz_input_value\n                    }\n                    quiz_field_name\n                    quiz_thank_you_msg\n                  }\n                  ... on FeedbackMultipleChoice {\n                    quiz_form_url\n                    quiz_title\n                    quiz_field_name\n                    quiz_extended_text\n                    quiz_thank_you_msg\n                    quiz_options {\n                      quiz_prompt\n                      quiz_input_type\n                      quiz_input_value\n                    }\n                  }\n                  ... on FeedbackText {\n                    text\n                  }\n                }\n              }\n              ... on Hotspot {\n                hotspot_description\n                hotspot_type\n                hotspot_file {\n                  ID\n                  url\n                  width\n                  height\n                }\n                hotspots {\n                  hotspot_coordinates\n                  hotspot_title\n                  hotspot_description\n                }\n              }\n              ... on Dragndrop {\n                intro_text\n                items {\n                  option\n                  target\n                  target_sound_file\n                }\n                origin_heading\n                target_heading\n              }\n              ... on ShortText {\n                form_title\n                form_field_name\n              }\n              ... on Checkbox {\n                quiz_title\n                quiz_field_name\n                quiz_thank_you_msg\n                quiz_options {\n                  quiz_input_value\n                  photo {\n                    ID\n                    url\n                  }\n                quiz_prompt\n                quiz_input_type\n              }\n            }\n          }\n        }\n      }\n    }\n  ';
  var variables = {
    postSlug: postSlug
  };

  var result = apolloFetch({ query: query, variables: variables }).then(function (res) {
    return res.data;
  });

  return result;
};



/***/ }),

/***/ "./api/resolvers.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__controllers__ = __webpack_require__("./api/controllers.js");


var resolvers = {
    Query: {
        posts: __WEBPACK_IMPORTED_MODULE_0__controllers__["c" /* posts */],
        postTitle: __WEBPACK_IMPORTED_MODULE_0__controllers__["b" /* postTitle */],
        postBySlug: __WEBPACK_IMPORTED_MODULE_0__controllers__["a" /* postBySlug */]
    },
    //the following is necessary to implement interfaces in apollo graphql
    Card: {
        __resolveType: function __resolveType(obj, context, info) {
            if (obj.acf_fc_layout === "intro") {
                return 'Intro';
            }

            if (obj.acf_fc_layout === "multiple_choice") {
                return 'MultipleChoice';
            }

            if (obj.acf_fc_layout === "text") {
                return 'Text';
            }

            if (obj.acf_fc_layout === "dragndrop") {
                return 'Dragndrop';
            }

            if (obj.acf_fc_layout === "feedback") {
                return 'FeedBack';
            }

            if (obj.acf_fc_layout === "likert") {
                return 'Likert';
            }

            if (obj.acf_fc_layout === "likert_grid") {
                return 'LikertGrid';
            }

            if (obj.acf_fc_layout === "hotspot") {
                return 'Hotspot';
            }

            if (obj.acf_fc_layout === "short_text") {
                return 'ShortText';
            }

            if (obj.acf_fc_layout === "checkbox") {
                return 'Checkbox';
            }

            if (obj.acf_fc_layout === "multiple_choice_multiple") {
                return 'MultipleChoiceMultiple';
            }

            if (obj.acf_fc_layout === "multiple_choice_grid") {
                return 'MultipleChoiceGrid';
            }

            return null;
        }
    },
    FeedbackType: {
        __resolveType: function __resolveType(obj, context, info) {

            if (obj.acf_fc_layout === "feedback_short_text") {
                return 'FeedbackShorText';
            }

            if (obj.acf_fc_layout === "feedback_likert_scale") {
                return 'FeedbackLikert';
            }

            if (obj.acf_fc_layout === "feedback_multiple_choice") {
                return 'FeedbackMultipleChoice';
            }

            if (obj.acf_fc_layout === "feedback_text") {
                return 'FeedbackText';
            }

            return null;
        }
    }
};

/* harmony default export */ __webpack_exports__["a"] = (resolvers);

/***/ }),

/***/ "./api/typeDefinitions.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var typeDefs = "\n  type Query {\n    posts: [Post]\n    postTitle: [PostTitle]\n    postBySlug(slug: String): [Post]\n  }\n\n  type PostTitle @cacheControl(maxAge: 240) {\n    ID: ID\n    post_name: String\n    post_title: String\n    slug: String\n    acf: Acf\n  }\n\n  type Post @cacheControl(maxAge: 240) {\n    id: ID\n    slug: String\n    acf: Acf\n  }\n\n  type Acf {\n    card_title: String\n    card_summary: String\n    fact_sheet: factSheetOptions\n    card_image: String\n    cards: [Card]\n    tabs: [Tab]\n    intro_text: String\n  }\n\n  type factSheetOptions {\n    url: String\n    title: String\n    filename: String\n    filesize: String\n    mime_type: String\n  }\n\n  type Tab {\n    heading: String\n    block_1: String\n    block_2: String\n  }\n\n  interface Card {\n    acf_fc_layout: String\n  }\n\n  type Text implements Card {\n    acf_fc_layout: String\n    type: String\n    text: String\n  }\n\n  type Intro implements Card {\n    acf_fc_layout: String\n    block_1: String\n    block_2: String\n    block_3: String\n    level: String\n    duration: String\n    classroom: Boolean\n  }\n\n  type MultipleChoice implements Card {\n    acf_fc_layout: String\n    quiz_form_url: String\n    quiz_title: String\n    quiz_extended_text: String\n    quiz_field_name: String\n    quiz_thank_you_msg: String\n    quiz_options: [InputOptions]\n  }\n\n  type Dragndrop implements Card {\n    acf_fc_layout: String\n    intro_text: String\n    items: [DragndropOptions]\n    origin_heading: String\n    target_heading: String\n  }\n\n  type DragndropOptions {\n    option: String\n    target: String\n    target_sound_file: String  \n  }\n\n  type MultipleChoiceMultiple implements Card {\n    acf_fc_layout: String\n    quiz_description: String\n    quiz: [Quiz]\n  }\n\n  type MultipleChoiceGrid implements Card {\n    acf_fc_layout: String\n    quiz_description: String\n    quiz: [QuizGrid]\n  }\n\n  type QuizGrid {\n    quiz_input_value: String\n    quiz_options: [QuizGridOptions]\n  }\n\n  type QuizGridOptions {\n    quiz_title: String\n    quiz_extended_text: String\n    quiz_field_name: String\n    quiz_tip: String\n  }\n\n  type Likert implements Card {\n    acf_fc_layout: String\n    quiz_title: String\n    quiz_field_name: String\n    quiz_thank_you_msg: String\n    quiz_options: [LikertOptions]\n  }\n\n  type LikertOptions {\n    quiz_input_value: String\n  }\n\n  type LikertGrid implements Card {\n    acf_fc_layout: String\n    quiz_title: String\n    quiz_thank_you_msg: String\n    quiz_options: [LikertGridOptions]\n  }\n\n  type LikertGridOptions {\n    quiz_question: String\n    quiz_field_name: String\n  }\n\n  type Hotspot implements Card {\n    acf_fc_layout: String\n    hotspot_description: String\n    hotspot_type: String\n    hotspots: [HotspotData]\n    hotspot_file: ImgOptions\n  }\n\n  type HotspotData {\n    hotspot_coordinates: String\n    hotspot_title: String\n    hotspot_description: String\n  }\n\n  type Checkbox implements Card {\n    acf_fc_layout: String\n    quiz_title: String\n    quiz_field_name: String\n    quiz_thank_you_msg: String\n    quiz_options: [InputOptions]\n  }\n\n  type ShortText implements Card {\n    acf_fc_layout: String\n    form_title: String\n    form_field_name: String\n  }\n\n  type FeedBack implements Card {\n    acf_fc_layout: String\n    feedback_type: [FeedbackType]\n  }\n\n  interface FeedbackType {\n    acf_fc_layout: String\n  }\n\n  type FeedbackShorText implements FeedbackType {\n    acf_fc_layout: String\n    form_url: String\n    form_description: String\n    form_field_name: String\n  }\n\n  type FeedbackLikert implements FeedbackType {\n    acf_fc_layout: String\n    quiz_title: String\n    quiz_field_name: String\n    quiz_thank_you_msg: String\n    quiz_options: [LikertOptions]\n  }\n\n  type FeedbackMultipleChoice implements FeedbackType {\n    acf_fc_layout: String\n    quiz_form_url: String\n    quiz_title: String\n    quiz_extended_text: String\n    quiz_field_name: String\n    quiz_thank_you_msg: String\n    quiz_options: [InputOptions]\n  }\n\n  type FeedbackText implements FeedbackType {\n    acf_fc_layout: String\n    text: String\n  }\n\n  type Quiz {\n    quiz_form_url: String\n    quiz_title: String\n    quiz_extended_text: String\n    quiz_field_name: String\n    quiz_thank_you_message: String\n    quiz_options: [QuizOptions]\n  }\n\n  type QuizOptions {\n    quiz_input_value: String\n    quiz_input_type: String\n  }\n\n  type InputOptions {\n    quiz_input_value: String\n    photo: ImgOptions\n    quiz_prompt: String\n    quiz_input_type: String\n  }\n\n  type ImgOptions {\n    ID: ID\n    url: String\n    title: String\n    width: String\n    height: String\n  }\n\n";

/* harmony default export */ __webpack_exports__["a"] = (typeDefs);

/***/ }),

/***/ "./index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_http__ = __webpack_require__("http");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_http__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__server__ = __webpack_require__("./server.js");



//set port
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

var port = normalizePort(Object({"NODE_ENV":"production","STAGING_ADMIN_URL":"https://ncwr-admin.cg-dev.eu","LOCAL_ADMIN_URL":"http://localhost/ncwr-admin","STAGING_GRAPHQL_URL":"https://ncwr.cg-dev.eu/graphql","LOCAL_GRAPHQL_URL":"http://localhost:3000/graphql"}).port || 3000);

//create server
var httpServer = __WEBPACK_IMPORTED_MODULE_0_http___default.a.createServer(__WEBPACK_IMPORTED_MODULE_1__server__["default"]);

var currentApp = __WEBPACK_IMPORTED_MODULE_1__server__["default"];

httpServer.listen(port, function () {
    console.log('Server is listening on port ' + port);
});

if (true) {
    module.hot.accept(["./server.js"], function(__WEBPACK_OUTDATED_DEPENDENCIES__) { /* harmony import */ __WEBPACK_IMPORTED_MODULE_1__server__ = __webpack_require__("./server.js"); (function () {
        httpServer.removeListener('request', currentApp);
        httpServer.on('request', __WEBPACK_IMPORTED_MODULE_1__server__["default"]);
        currentApp = __WEBPACK_IMPORTED_MODULE_1__server__["default"];
    })(__WEBPACK_OUTDATED_DEPENDENCIES__); });
}

/***/ }),

/***/ "./node_modules/webpack/hot/log-apply-result.js":
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
module.exports = function(updatedModules, renewedModules) {
	var unacceptedModules = updatedModules.filter(function(moduleId) {
		return renewedModules && renewedModules.indexOf(moduleId) < 0;
	});
	var log = __webpack_require__("./node_modules/webpack/hot/log.js");

	if(unacceptedModules.length > 0) {
		log("warning", "[HMR] The following modules couldn't be hot updated: (They would need a full reload!)");
		unacceptedModules.forEach(function(moduleId) {
			log("warning", "[HMR]  - " + moduleId);
		});
	}

	if(!renewedModules || renewedModules.length === 0) {
		log("info", "[HMR] Nothing hot updated.");
	} else {
		log("info", "[HMR] Updated modules:");
		renewedModules.forEach(function(moduleId) {
			if(typeof moduleId === "string" && moduleId.indexOf("!") !== -1) {
				var parts = moduleId.split("!");
				log.groupCollapsed("info", "[HMR]  - " + parts.pop());
				log("info", "[HMR]  - " + moduleId);
				log.groupEnd("info");
			} else {
				log("info", "[HMR]  - " + moduleId);
			}
		});
		var numberIds = renewedModules.every(function(moduleId) {
			return typeof moduleId === "number";
		});
		if(numberIds)
			log("info", "[HMR] Consider using the NamedModulesPlugin for module names.");
	}
};


/***/ }),

/***/ "./node_modules/webpack/hot/log.js":
/***/ (function(module, exports) {

var logLevel = "info";

function dummy() {}

function shouldLog(level) {
	var shouldLog = (logLevel === "info" && level === "info") ||
		(["info", "warning"].indexOf(logLevel) >= 0 && level === "warning") ||
		(["info", "warning", "error"].indexOf(logLevel) >= 0 && level === "error");
	return shouldLog;
}

function logGroup(logFn) {
	return function(level, msg) {
		if(shouldLog(level)) {
			logFn(msg);
		}
	};
}

module.exports = function(level, msg) {
	if(shouldLog(level)) {
		if(level === "info") {
			console.log(msg);
		} else if(level === "warning") {
			console.warn(msg);
		} else if(level === "error") {
			console.error(msg);
		}
	}
};

var group = console.group || dummy;
var groupCollapsed = console.groupCollapsed || dummy;
var groupEnd = console.groupEnd || dummy;

module.exports.group = logGroup(group);

module.exports.groupCollapsed = logGroup(groupCollapsed);

module.exports.groupEnd = logGroup(groupEnd);

module.exports.setLogLevel = function(level) {
	logLevel = level;
};


/***/ }),

/***/ "./node_modules/webpack/hot/poll.js?1000":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__resourceQuery) {/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
/*globals __resourceQuery */
if(true) {
	var hotPollInterval = +(__resourceQuery.substr(1)) || (10 * 60 * 1000);
	var log = __webpack_require__("./node_modules/webpack/hot/log.js");

	var checkForUpdate = function checkForUpdate(fromUpdate) {
		if(module.hot.status() === "idle") {
			module.hot.check(true).then(function(updatedModules) {
				if(!updatedModules) {
					if(fromUpdate) log("info", "[HMR] Update applied.");
					return;
				}
				__webpack_require__("./node_modules/webpack/hot/log-apply-result.js")(updatedModules, updatedModules);
				checkForUpdate(true);
			}).catch(function(err) {
				var status = module.hot.status();
				if(["abort", "fail"].indexOf(status) >= 0) {
					log("warning", "[HMR] Cannot apply update.");
					log("warning", "[HMR] " + err.stack || err.message);
					log("warning", "[HMR] You need to restart the application!");
				} else {
					log("warning", "[HMR] Update failed: " + err.stack || err.message);
				}
			});
		}
	};
	setInterval(checkForUpdate, hotPollInterval);
} else {
	throw new Error("[HMR] Hot Module Replacement is disabled.");
}

/* WEBPACK VAR INJECTION */}.call(exports, "?1000"))

/***/ }),

/***/ "./server.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(__dirname) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__("babel-runtime/regenerator");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator__ = __webpack_require__("babel-runtime/helpers/asyncToGenerator");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_typeDefinitions__ = __webpack_require__("./api/typeDefinitions.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_resolvers__ = __webpack_require__("./api/resolvers.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__api_graphqlRouter__ = __webpack_require__("./api/graphqlRouter.js");



var _this = this;

var express = __webpack_require__("express");
var path = __webpack_require__("path");
var bodyParser = __webpack_require__("body-parser");
var logger = __webpack_require__("morgan");
var nunjucks = __webpack_require__("nunjucks");

var _require = __webpack_require__("apollo-server-express"),
    graphqlExpress = _require.graphqlExpress,
    graphiqlExpress = _require.graphiqlExpress;

var _require2 = __webpack_require__("graphql-tools"),
    makeExecutableSchema = _require2.makeExecutableSchema;

var compression = __webpack_require__("compression");
var webpack = __webpack_require__("webpack");
var webpackDevMiddleware = __webpack_require__("webpack-dev-middleware");
var config = __webpack_require__("./webpack.common.js");
var compiler = webpack(config);
var asyncHandler = __webpack_require__("express-async-handler");
var favicon = __webpack_require__("serve-favicon");

// Initialize the app
var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));

nunjucks.configure('./views', {
  express: app,
  autoescape: false,
  noCache: false,
  watch: true
});
app.set('view engine', 'njk');

//middleware
//json parsing
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//compress files
app.use(compression());
// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
app.use(webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath
}));
//favicon
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//logging
app.use(logger('dev'));
//public path
app.use('/static', express.static(path.join(__dirname, 'public')));

// GraphQl stuff
// schema imports


// The schema
var schema = makeExecutableSchema({
  typeDefs: __WEBPACK_IMPORTED_MODULE_2__api_typeDefinitions__["a" /* default */],
  resolvers: __WEBPACK_IMPORTED_MODULE_3__api_resolvers__["a" /* default */]
});
// The GraphQL endpoint
app.use('/graphql', bodyParser.json(), graphqlExpress({
  schema: schema,
  context: {},
  tracing: true,
  cacheControl: true
}));
// GraphiQL, visual editor for queries
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

// routes


app.get('/', asyncHandler(function () {
  var _ref = __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator___default()( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee(req, res, next) {
    var postTitles, posts;
    return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return Object(__WEBPACK_IMPORTED_MODULE_4__api_graphqlRouter__["b" /* getPostTitles */])();

          case 2:
            postTitles = _context.sent;
            _context.next = 5;
            return Object(__WEBPACK_IMPORTED_MODULE_4__api_graphqlRouter__["c" /* getPosts */])();

          case 5:
            posts = _context.sent;


            res.render('index', {
              postList: postTitles.postTitle,
              posts: posts.posts
            });

          case 7:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, _this);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}()));

app.get('/post/:slug/', asyncHandler(function () {
  var _ref2 = __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator___default()( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee2(req, res, next) {
    var postTitles, postBySlug, posts;
    return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return Object(__WEBPACK_IMPORTED_MODULE_4__api_graphqlRouter__["b" /* getPostTitles */])();

          case 2:
            postTitles = _context2.sent;
            _context2.next = 5;
            return Object(__WEBPACK_IMPORTED_MODULE_4__api_graphqlRouter__["a" /* getPostBySlug */])(req.params.slug);

          case 5:
            postBySlug = _context2.sent;
            _context2.next = 8;
            return Object(__WEBPACK_IMPORTED_MODULE_4__api_graphqlRouter__["c" /* getPosts */])();

          case 8:
            posts = _context2.sent;

            res.render('post', {
              post: postBySlug.postBySlug,
              postList: postTitles.postTitle,
              posts: posts.posts
            });

          case 10:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));

  return function (_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}()));

/* harmony default export */ __webpack_exports__["default"] = (app);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, ""))

/***/ }),

/***/ "./webpack.common.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {var webpack = __webpack_require__("webpack");
var path = __webpack_require__("path");
//clean up dist folder
var CleanWebpackPlugin = __webpack_require__("clean-webpack-plugin");
var StartServerPlugin = __webpack_require__("start-server-webpack-plugin");
var merge = __webpack_require__("webpack-merge");
var UglifyJsPlugin = __webpack_require__("uglifyjs-webpack-plugin");

module.exports = {
    entry: {
        polyfills: ['./src/js/polyfills.js'],
        app: ['./src/js/app.js']
    },
    module: {
        rules: [{
            test: /\.js?$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    babelrc: false,
                    presets: [['env', { modules: false }], 'stage-0'],
                    plugins: ['transform-regenerator', 'transform-runtime']
                }
            }],
            exclude: /node_modules/
        }, {
            test: /\.css$/,
            use: ['style-loader']
        }, {
            test: /\.(png|svg|jpg|gif)$/,
            use: ['file-loader']
        }, {
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            use: ['file-loader']
        }]
    },
    plugins: [
        // new UglifyJsPlugin()
    ],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/static'
    }
};
/* WEBPACK VAR INJECTION */}.call(exports, ""))

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./node_modules/webpack/hot/poll.js?1000");
module.exports = __webpack_require__("./index.js");


/***/ }),

/***/ "apollo-fetch":
/***/ (function(module, exports) {

module.exports = require("apollo-fetch");

/***/ }),

/***/ "apollo-server-express":
/***/ (function(module, exports) {

module.exports = require("apollo-server-express");

/***/ }),

/***/ "babel-runtime/helpers/asyncToGenerator":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/asyncToGenerator");

/***/ }),

/***/ "babel-runtime/regenerator":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/regenerator");

/***/ }),

/***/ "body-parser":
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "clean-webpack-plugin":
/***/ (function(module, exports) {

module.exports = require("clean-webpack-plugin");

/***/ }),

/***/ "compression":
/***/ (function(module, exports) {

module.exports = require("compression");

/***/ }),

/***/ "express":
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "express-async-handler":
/***/ (function(module, exports) {

module.exports = require("express-async-handler");

/***/ }),

/***/ "graphql-tools":
/***/ (function(module, exports) {

module.exports = require("graphql-tools");

/***/ }),

/***/ "http":
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),

/***/ "morgan":
/***/ (function(module, exports) {

module.exports = require("morgan");

/***/ }),

/***/ "nunjucks":
/***/ (function(module, exports) {

module.exports = require("nunjucks");

/***/ }),

/***/ "path":
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),

/***/ "serve-favicon":
/***/ (function(module, exports) {

module.exports = require("serve-favicon");

/***/ }),

/***/ "start-server-webpack-plugin":
/***/ (function(module, exports) {

module.exports = require("start-server-webpack-plugin");

/***/ }),

/***/ "uglifyjs-webpack-plugin":
/***/ (function(module, exports) {

module.exports = require("uglifyjs-webpack-plugin");

/***/ }),

/***/ "webpack":
/***/ (function(module, exports) {

module.exports = require("webpack");

/***/ }),

/***/ "webpack-dev-middleware":
/***/ (function(module, exports) {

module.exports = require("webpack-dev-middleware");

/***/ }),

/***/ "webpack-merge":
/***/ (function(module, exports) {

module.exports = require("webpack-merge");

/***/ })

/******/ });
//# sourceMappingURL=server.js.map