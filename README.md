# Sample App
An Express.js app, using a GraphQL REST API wrapper (endpoints from http://jsonplaceholder.typicode.com/) to retrieve users, posts and comments.

## Protocol
- HTTPS, HTTP/2
  - spdy
## Package management
- webpack
## Task management
- gulp 
  - gulp-sass
## Data
- GraphQL: 
  - apollo-server-express 
    - graphqlExpress
    - graphiqlExpress
  - apollo-fetch
  - graphql-tools
## JavaScript
- es6
- lodash
- Fetch API
## CSS
- Grid Layout
## Performance Checkpoints
- HTTPS, HTTP/2 
- HTTP/2 server push for render-blocking stylesheets
- font preloading
- module pattern
- text compression
- minify CSS and JS
## Browser Support
- Last 2 versions (IE11 & IE10)

## To use:

`npm install`

`npm run start`

Server running on https://localhost/3001

## TODO:

- error handling
- tests

## KIOSK ID's
Kiosk 1: 6
Kiosk 2: 45

Change the kiosk id in ./config.json and restart the application
`npm run killNode`
`npm run start`

## electron 
run `electron .` to start the app in electron
run `npm run start`to start the app in default browser (chrome)


#graphql
to upload a change to the API, test in graphiql, if it returns a value it's safe to upload to the server, then log in to the server, restart the app with pm2 restart server
- pm2 list
- pm2 monit
- pm2 restart server 