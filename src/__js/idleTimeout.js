"use strict";

import State from './state.js'

const IdleTimeout = {
    settings() {
        this.last_movement = 0;
        this.check_interval_timeout = 1000; //1000
        this.pre_idle_seconds = 1; //45
        this.idle_seconds = parseInt(this.pre_idle_seconds) + 15;
    },
    init() {
        this.settings();
        //set idle state to none on load
        this.resetIdleTimeout();
        //every second check the time elapsed from last movement
        clearInterval(this.checkIdleInterval());
        //reset idle timeout on pointer events
        this.addListeners();
    },
    checkIdleInterval() {
        //when time hits pre idle & idle seconds, if video not playing, change app state
        this.check_interval = setInterval(() => {
            if (State.getVideoState() === 'video_state_true') {
                this.resetIdleTimeout();
            } else {
                this.checkIdleTimeout();
            }
        }, this.check_interval_timeout);
    },
    addListeners() {
        //when any of the following events fire, resetIdleTimeout is called
        //resetIdleTimeout records the time of the last activity/pointerevent 
        //and sets idle state to none
        document.getElementById('app').addEventListener('pointerdown', (() => {
            this.resetIdleTimeout();
        }), { passive: true })

        document.getElementById('app').addEventListener('pointerup', (() => {
            this.resetIdleTimeout();
        }), { passive: true })

        document.getElementById('app').addEventListener('click', (() => {
            this.resetIdleTimeout();
        }), { passive: true })

        document.getElementById('app').addEventListener('touchmove', (() => {
            this.resetIdleTimeout();
        }), { passive: true })

        document.getElementById('app').addEventListener('touchstart', (() => {
            this.resetIdleTimeout();
        }), { passive: true })

        document.getElementById('app').addEventListener('touch', (() => {
            this.resetIdleTimeout();
        }), { passive: true })
    },
    resetIdleTimeout() {
        this.last_movement = new Date().getTime();
        State.setIdleState('idle_state_none'); 
    },
    checkIdleTimeout() {
        //record current time (checkIdleTimeout is called every second in init)
        this.currentTime = new Date().getTime();
        //record how long between current time and last pointer event
        var dt = this.currentTime - this.last_movement;
        //get elapsed time in seconds
        var dt_seconds = dt / 1000;
        //if time from last pointer event exceeds idle_seconds (is set in init())
        //and if the video is not playing
        if (dt_seconds >= this.idle_seconds) {
            //set idle state to idle  
            State.setIdleState('idle_state_idle');
            //if time from last pointer event exceeds pre_idle_seconds (set in init())
        } else if (dt_seconds >= this.pre_idle_seconds) {
            //set idle state to pre idle
            State.setIdleState('idle_state_pre_idle');
        } 
    }

}

export default IdleTimeout