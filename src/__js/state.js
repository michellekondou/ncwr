"use strict"

const State = {
    init() {
        this.idle_state = null;
        this.attract_state = null;
        this.video_state = null;
        this.screen_state = null;
    },
    applyStateClass(state, root, regex) {
        //check if the html element contains the root (e.g. 'attract_state') of the class we want to apply (e.g. 'attract_state_true')
        let el = document.querySelector('html[class*="' + root + '"]')
        if(el) {
            //if so, remove only the specific class (e.g. 'attract_state_true'), not any other classes
            el.classList.remove(el.className.match(regex))
        }
        //then, add the new state class to the html element
        if (state != null) {
            document.getElementsByTagName('html')[0].classList.add(state)
        }
    },
    setAttractState(state) {
        if (this.attract_state == state) {
            return;
        }
        this.attract_state = state;
        this.applyStateClass(state, 'attract_state', /\battract_state_\S+/g);

        // if (this.getAttractState() === 'attract_state_true') {
        //     ViewContainer.settings.call(this);
        //     this.attract_video[0].play();
        //     var video = $('.l-video #video-layout');
        //     if (video.length >= 1 && this.getVideoState() === 'video_state_true') {
        //          Page.handleVideo('stop');
        //     }
        //     $('.slides').cycle('resume');
        // } else {
        //     this.attract_video[0].pause();
        //     this.setVideoState('video_state_false');
        //     $('.slides').cycle('pause'); 
        // }
    },
    getAttractState() {
        console.log(this.attract_state)
        return this.attract_state;
    },
    setIdleState(state) {
        if (this.idle_state == state) {
            return;
        }
        this.idle_state = state;

        this.applyStateClass(state, 'idle_state', /\bidle_state_\S+/g);

        // if (this.getAttractState() === 'attract_state_false' && this.getVideoState() === 'video_state_false') {
        //     if (this.getIdleState() === 'idle_state_pre_idle') {
        //         Page.openIdleTimeoutPopup();
        //     }
        //     if (this.getIdleState() === 'idle_state_idle') {
        //         Page.clearIdleTimeoutPopupTimer();
        //         Page.fadeIdleTimeoutPopup();
        //         Page.closePageReturnToAttract();
        //     }
        // }

        // if (this.getIdleState() === 'idle_state_none'){
        //     Page.clearIdleTimeoutPopupTimer();
        //     Page.fadeIdleTimeoutPopup();
        // }
    },
    getIdleState() {
        return this.idle_state;
    },
    setVideoState(state) {
        if (this.video_state == state) {
            return;
        }
        this.video_state = state;
        this.applyStateClass(state, 'video_state', /\bvideo_state_\S+/g);
    },
    getVideoState() {
        return this.video_state;
    },
    setScreenState(state) {
        if (this.screen_state == state) {
            return;
        }
        this.screen_state = state;
        this.applyStateClass(state, 'inner_state', /\inner_state_\S+/g);
    },
    getScreenState() {
        return this.screen_state;
    },
}

export default State