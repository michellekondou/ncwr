"use strict";

import State from './state.js'
import IdleTimeout from './idleTimeout.js'
import MainScreen from './mainScreen.js'

const WebApp = {
    init: function () {
        console.log('starting app')
        //handle application state
        State.init()
        //handle application idleTimeout
        IdleTimeout.init()
        //handle main app screen
        MainScreen.init()
        //handle app scrolling
        window.addEventListener('wheel', function () { return false }, { passive: true });
        document.getElementById('app').addEventListener('touchmove', function (e) {
            //only allow scolling via touch in selected areas like the slideup boxes
            // if (!$('.scroll-container .text').has($(e.target)).length) {
            //     e.preventDefault();
            // }
            e.preventDefault();
        }, { passive: false });

    }
};

//WebApp.init();










