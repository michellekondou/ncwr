"use strict"

//const { sortBy, split, zipObject, filter } = require('lodash');
import { hasClass } from './helpers.js'
import State from './state.js'

const MainScreen = {
    init() {
        //set dom elements
        this.main_screen = document.getElementById('app-screen-main')
        this.nav = document.getElementById('app-main__nav')
        this.nav_buttons = this.nav.querySelectorAll('button')
        this.inner_screens = document.querySelectorAll('.app-screen')
        this.inner_screen_active = document.querySelector('.current')
        this.backButton = document.querySelectorAll('.button--back')

        this.openScreen()
        this.closeScreen()

    },
    openScreen() {
        this.nav_buttons.forEach(el => el.addEventListener('pointerup', ((e) => {
            //find the nav button target screen based on the data-target attribute
            Array.from(this.inner_screens).filter(el => {
                el.classList.add('visually-hidden')
                el.classList.remove('current')
                if (hasClass(el, e.target.getAttribute('data-target'))) {
                    el.classList.toggle('visually-hidden')
                    el.classList.toggle('current')
                    State.setScreenState('inner_state_true')
                }
            })
        })))
    },
    closeScreen() {
        this.backButton.forEach(el => el.addEventListener('pointerup', (() => {
            //find the currently visible screen
            Array.from(this.inner_screens).filter(el => {
                if (hasClass(el, 'current')) {
                    el.classList.remove('current')
                    el.classList.add('visually-hidden')
                    State.setScreenState('inner_state_false')
                }
            })
            this.main_screen.classList.remove('visually-hidden')
        })))
    }
}

export default MainScreen