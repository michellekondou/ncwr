"use strict"

import * as d3 from '../../node_modules/d3/dist/d3.min.js'
import { TimelineLite, TimelineMax } from 'gsap/TweenMax';
import Page from './page'
import DranAndDrop from './dndOneToOne'
import Hotspot from './hotspot'
import Navigation from './navigation'
import { hasClass } from './helpers.js';
//import * as d3_tip from '../../node-modules/d3-tip/dist/index.js'

const Map = {
    init() {
        this.client_width = document.documentElement.clientWidth
        this.client_height = document.documentElement.clientHeight
        this.svg_object = document.getElementById('map')
        this.mapStatic = document.querySelector('.map-static')

        let map_src

        const resizeMaps = (e) => {
            if (document.documentElement.clientWidth > 1680) {
                map_src = '/static/media/new/ncwr-map-1920x1080.svg'
                this.obj_width = 1920
                this.obj_height = 1080
            } else if (document.documentElement.clientWidth > 1600) {
                map_src = '/static/media/new/ncwr-map-1680x1050.svg'
                this.obj_width = 1680
                this.obj_height = 1050
            } else if (document.documentElement.clientWidth > 1536) {
                map_src = '/static/media/new/ncwr-map-1600x900.svg'
                this.obj_width = 1600
                this.obj_height = 900
            } else if (document.documentElement.clientWidth > 1440) {
                map_src = '/static/media/new/ncwr-map-1536x864.svg'
                this.obj_width = 1536
                this.obj_height = 864
            } else if (document.documentElement.clientWidth > 1366) {
                map_src = '/static/media/new/ncwr-map-1440x900.svg'
                this.obj_width = 1440
                this.obj_height = 900
            } else if (document.documentElement.clientWidth > 1280) {
                map_src = '/static/media/new/ncwr-map-1366x768.svg'
                this.obj_width = 1366
                this.obj_height = 768
            } else if (document.documentElement.clientWidth > 1200) {
                map_src = '/static/media/new/ncwr-map-1280x800.svg'
                this.obj_width = 1280
                this.obj_height = 800
            } 
            // else if (document.documentElement.clientWidth > 960) {
            //     map_src = '/static/media/new/ncwr-map-1200x800.svg'
            //     this.obj_width = 1200
            //     this.obj_height = 800
            // }

            if (this.svg_object) {
                this.getMapObject(map_src)
            }
        }
        this.viewport_width = this.client_width
        this.viewport_height = this.client_height;
        this.lpw = this.client_width
        this.lph = this.client_height
        
        var parent = this
        if (document.documentElement.clientWidth > 1199) {
            window.addEventListener('resize', resizeMaps, true)
            resizeMaps()
            if (this.svg_object) {
                this.svg_object.addEventListener("load", (e) => {
                    this.map_bcr = this.svg_object.getBoundingClientRect()
                    this.getMapElements()
                    this.createPoints()
                    const preloader = document.getElementById('preloader')
                    const loader = document.querySelector('.loading')
                    setTimeout(() => {
                        const load_tl = new TimelineLite({
                            onComplete: function () {
                                loader.classList.add('hidden')
                                setTimeout(() => {
                                    if (window.location.pathname == '/') {
                                        this.svg_object = document.getElementById('map')
                                        this.map_bcr = this.svg_object.getBoundingClientRect()
                                        this.svg_doc = document.getElementById('map').contentDocument
                                        this.svg_element = this.svg_doc.getElementById("svgmap")
                                        this.view_element = this.svg_doc.getElementById("view")
                                        const tooltips = document.querySelectorAll('.map-tooltip')
                                        let svg = d3.select(this.svg_element)
                                        const points = svg.selectAll('.point').nodes()
                                        points.map((d) => {
                                            if (d.getAttribute('id') === 'where-to-begin') {
                                                Array.from(tooltips).forEach(tooltip => {
                                                    if (tooltip.getAttribute('data-id') === 'where-to-begin') {
                                                        TweenMax.set(tooltip, {
                                                            top: d.getBoundingClientRect().top - this.map_bcr.top - 30 + 'px',
                                                            left: d.getBoundingClientRect().left - tooltip.getBoundingClientRect().width / 2 + 'px'
                                                        })

                                                        TweenMax.to(tooltip, 0.3, {
                                                            top: d.getBoundingClientRect().top + this.map_bcr.top + d.getBoundingClientRect().height + 10 + 'px',
                                                            ease: Power2.easeOut,
                                                            delay: 2,
                                                            lazy: true,
                                                            onStart: function () {
                                                                tooltip.classList.add('visible')
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })

                                        parent.animate()
                                    }
                                }, 500)
                            }
                        })
                        if (preloader) {
                            load_tl
                                .to(document.getElementById('preloader'), 0.3, {
                                    opacity: 0,
                                    zIndex: -1
                                })
                                .set(this.svg_object, {
                                    visibility: 'visible'
                                })
                        } else {
                            load_tl
                                .set(this.svg_object, {
                                    visibility: 'visible'
                                })
                        }
                    }, 1200)
                }, false)
            }
        } else {
            console.log('here')
            this.svg_object.remove()
            document.querySelector('.loading').classList.add('hidden')
            TweenMax
                .to(document.getElementById('preloader'), 0.3, {
                    opacity: 0,
                    zIndex: -1
                })
            return
        }
    },
    getMapObject(map_src) {
        this.svg_object.setAttribute('data', map_src)
        this.svg_object.setAttribute('style', 'position:absolute;visibility:hidden;touch-action:none;top: 50%;left: 50%;transform: translate(-50%, -50%);width:' + this.obj_width + 'px;height:' + this.obj_height + 'px')
    },
    getMapElements() {
        this.svg_doc = this.svg_object.contentDocument
        this.svg_element = this.svg_doc.getElementById("svgmap")
        this.view_element = this.svg_doc.getElementById("view")

        let svg = d3.select(this.svg_element)
        let width = +svg.attr("width")
        let height = +svg.attr("height")

        const view = d3.select(this.view_element)
                    .attr("class", "view")
                    .attr("x", 0.5)
                    .attr("y", 0.5)
                    .attr("width", width + 'px')
                    .attr("height", height + 'px')
                    .style("pointer-events", "all")

        function zoomed() {
            var duration
            var map_item

            //change cursor according to mouse event
            if (d3.event.sourceEvent !== null) {

                if (d3.event.sourceEvent.ctrlKey) {
                    return;
                }
                //detects zoom-in
                if (d3.event.sourceEvent.deltaY < 0) {
                    svg.style("cursor", "zoom-in");
                    //detects zoom-out
                } else if (d3.event.sourceEvent.deltaY > 0) {
                    svg.style("cursor", "zoom-out");
                }
                //detects panning
                if (d3.event.sourceEvent.movementX != 0 || d3.event.sourceEvent.movementY != 0) {
                    svg.style("cursor", "move");
                    duration = 0
                }
            }

            //handle zoom
            // view.transition()
            //     .duration(duration)
            //     .attr("transform", "translate(" + d3.event.transform.x + "," + d3.event.transform.y + ")" + " scale(" + d3.event.transform.k + ")");

            view.zoomLevel = d3.event.transform.k
            //handle zoom
            view.attr("transform", "translate(" + d3.event.transform.x + "," + d3.event.transform.y + ")" + " scale(" + d3.event.transform.k + ")")
        }

        function zoomEnd() {
            svg.transition().delay(1500).style("cursor", "default");
        }

        var svgWidth = d3.select(this.svg_element).attr("width");
        var svgHeight = d3.select(this.svg_element).attr("height");

        var t = 0,
            l = 0,
            b = svgWidth,
            r = svgHeight;

        const zoom = d3.zoom()
            .scaleExtent([1, 10])
            .translateExtent([[t, l], [b, r]])
            .on("zoom", zoomed)
            .on("zoom.end", zoomEnd);

        svg.call(zoom)

        const popups = document.querySelectorAll('.map-popup')

        if (popups) {
            Array.from(popups).forEach(popup => {
                var close_btn = popup.querySelector('.close-popup-btn')
                close_btn.addEventListener('pointerdown', (e) => {
                    resetted()
                })
            })
        }

        //controls
        const resetted = () => {
            svg.transition()
                .duration(500)
                .call(zoom.transform, d3.zoomIdentity);
        }

        const zoomIn = () => {
            svg.transition()
                .duration(250)
                .call(zoom.scaleBy, 2);
        }

        const zoomOut = () => {
            svg.transition()
                .duration(250)
                .call(zoom.scaleBy, 0.5);
        }
        if (document.getElementById('reset-zoom')) {
            document.getElementById('reset-zoom').addEventListener('click', (e) => {
                resetted()
                if (popups) {
                    Array.from(popups).forEach(popup => {
                        popup.classList.remove('visible')
                    })
                }
            }, { passive: true })
            document.getElementById('zoom-in').addEventListener('click', (e) => {
                zoomIn()
            }, { passive: true })
            document.getElementById('zoom-out').addEventListener('click', (e) => {
                zoomOut()
            }, { passive: true })

            document.getElementById('info-icon').addEventListener('click', (e) => {
                //document.querySelector('.app-screen--info').classList.add('open')
                e.preventDefault()
                const loader = document.querySelector('.loading')
                loader.classList.remove('hidden')

                const openPage = document.querySelector('.app-screen.open')
                if (openPage && !hasClass(openPage, 'app-screen--info')) {
                    Page.closePage(openPage)
                }
                
                const reqURL = e.target.getAttribute('href')
                if (reqURL === '') {
                    return
                }
                //request page
                Navigation.requestInfopage(reqURL)
                //push history state
                Navigation.appPushState(reqURL)

            }, { passive: false })
        }

        var zoomLevel = svg.call(zoom);
        return zoomLevel._groups[0][0].__zoom.k
    },
    createPoints() {
        let svg = d3.select(this.svg_element)
        const points = svg.selectAll('.point').nodes()
        const tooltips = document.querySelectorAll('.map-tooltip')
        const popups = document.querySelectorAll('.map-popup')
        let width = +svg.attr("width")
        let height = +svg.attr("height")

        const view = d3.select(this.view_element)
            .attr("class", "view")
            .attr("x", 0.5)
            .attr("y", 0.5)
            .attr("width", width - 1)
            .attr("height", height - 1)
            .style("pointer-events", "all")

        function zoomed() {
            var duration
            //change cursor according to mouse event
            if (d3.event.sourceEvent !== null) {

                if (d3.event.sourceEvent.ctrlKey) {
                    return;
                }
                //detects zoom-in

                if (d3.event.sourceEvent.deltaY < 0) {
                    svg.style("cursor", "zoom-in");
                    //if mobile
                    // if (cw < 1024) {
                    //     duration = 0;
                    // } else {
                    //     duration = 0;
                    // }
                    Array.from(popups).forEach(popup => {
                        popup.classList.remove('visible')
                    })

                    Array.from(tooltips).forEach(tooltip => {
                        tooltip.classList.remove('visible')
                    })
                    //detects zoom-out
                } else if (d3.event.sourceEvent.deltaY > 0) {
                    svg.style("cursor", "zoom-out");
                    //if mobile
                    // if (cw < 1024) {
                    //     duration = 0;
                    // } else {
                    //     duration = 0;
                    // }
                    Array.from(popups).forEach(popup => {
                        popup.classList.remove('visible')
                    })

                    Array.from(tooltips).forEach(tooltip => {
                        tooltip.classList.remove('visible')
                    })
                }
                //detects panning
                if (d3.event.sourceEvent.movementX != 0 || d3.event.sourceEvent.movementY != 0) {
                    svg.style("cursor", "move");
                    duration = 0

                    Array.from(popups).forEach(popup => {
                        popup.classList.remove('visible')
                    })

                    Array.from(tooltips).forEach(tooltip => {
                        tooltip.classList.remove('visible')
                    })

                }
            }

            //handle zoom
            view.transition()
                .duration(duration)
                .attr("transform", "translate(" + d3.event.transform.x + "," + d3.event.transform.y + ")" + " scale(" + d3.event.transform.k + ")");
        }

        function zoomEnd() {
            svg.transition().delay(1500).style("cursor", "default");
        }

        var t = 0,
            l = 0,
            b = width,
            r = height;
        var zoom = d3.zoom()
            .scaleExtent([1, 6])
            .translateExtent([[t, l], [b, r]])
            .on("zoom", zoomed)
            .on("zoom.end", zoomEnd)

        svg.call(zoom)

        let fadeOutTm

        points.map((d) => { 
            let zoomLevel = svg.call(zoom)._groups[0][0].__zoom.k
            let point_x = d.getBoundingClientRect().left
            let point_y = d.getBoundingClientRect().top

            d.addEventListener('mouseenter', (e) => {
                d.style.cursor = 'pointer'
                
                Array.from(tooltips).forEach(tooltip => {
                    
                    if (d.getAttribute('id') === tooltip.getAttribute('data-id') && d.getAttribute('id') !== 'where-to-begin') {
                        tooltip.style.top = 0
                        tooltip.style.left = 0
                        
                        if (d.getBoundingClientRect().top <= 120) {
                            tooltip.style.top = d.getBoundingClientRect().top + this.map_bcr.top + 50 + 'px' //50 = point height
                        } else {
                            tooltip.style.top = d.getBoundingClientRect().top + this.map_bcr.top - 50 + 'px' //50 = point height
                        }

                        if (d.getBoundingClientRect().left <= 120) {
                            tooltip.style.left = d.getBoundingClientRect().left + this.map_bcr.left - (tooltip.clientWidth / 2) + 16 + 'px' //32 = point width / 2
                        } else {
                            tooltip.style.left = d.getBoundingClientRect().left + this.map_bcr.left - (tooltip.clientWidth / 2) + 16 + 'px' //32 = point width / 2
                        }
                        
                        tooltip.classList.add('visible')
                    } else if (d.getAttribute('id') === tooltip.getAttribute('data-id') && d.getAttribute('id') === 'where-to-begin') {
                        if (!hasClass(tooltip), 'visible') {
                            console.log('not visible')
                        } else {
                            tooltip.style.top = d.getBoundingClientRect().top + 'px'
                            tooltip.style.left = d.getBoundingClientRect().left + this.map_bcr.left - (tooltip.clientWidth / 2) + 16 + 'px' 
                        }
                        tooltip.classList.add('visible')
                    }
                })
            })

            
            d.addEventListener('mouseleave', (e) => {
               
                Array.from(tooltips).forEach(tooltip => {
                    if (d.getAttribute('id') === tooltip.getAttribute('data-id')) {
                        fadeOutTm = setTimeout(() => {
                            tooltip.classList.remove('visible')
                        }, 500)
                    }
                })
            })

            d.addEventListener('click', (e) => {
                TweenMax.killAll()
                if (zoomLevel < 6) {
                    e.stopPropagation()
                    //center and zoom point
                    let t = d3.zoomIdentity.translate(width / 3, height / 3).scale(3).translate(-point_x + 16, -point_y - 25)
                    svg.transition().duration(500).call(zoom.transform, t)
                    const sidebar = document.querySelector('.sidebar-nav')
                    if (hasClass(sidebar, 'sidebar-nav--visible')) {
                        Navigation.openNav()
                    }
                  
                    //hide tooltip
                    Array.from(tooltips).forEach(tooltip => {
                        tooltip.classList.remove('visible')
                    })
                    //open popup
                    Array.from(popups).forEach(popup => {
                        popup.classList.remove('visible')
                        if (d.getAttribute('id') === popup.getAttribute('data-id')) {
                            popup.classList.add('visible')
                            const openPageBtn = popup.querySelector('.open-page')
                        }
                    })
                } 
            }, { passive: true })

            const points_tl = new TimelineMax({
                repeat: -1
            })

            // points_tl.to(d, 0.6, {
            //     y: -50
            // }).to(d, 0.6, {
            //     y: 0
            // })
         
        })

        
    },
    resetMapZoom() {
        console.log('reset map')
        this.svg_object = document.getElementById('map')
        this.svg_element = this.svg_doc.getElementById("svgmap")
        let svg = d3.select(this.svg_element)
        let width = +svg.attr("width")
        let height = +svg.attr("height")
        function zoomed() {
            //handle zoom
            view.attr("transform", "translate(" + d3.event.transform.x + "," + d3.event.transform.y + ")" + " scale(" + d3.event.transform.k + ")");
        }
        const view = d3.select(this.view_element)

        var zoom = d3.zoom().on("zoom", zoomed)

        svg.transition()
        .duration(150)
        .call(zoom.transform, d3.zoomIdentity)
    },
    animate() {
        let svg = d3.select(this.svg_element)
        const car = svg.select('#car').nodes()
        const sun_rays = svg.select('#sun-rays').nodes()
        const truck = svg.select('#track').nodes()
        const airballoon = svg.select('#airballoon').nodes()
        const windmills = svg.selectAll('.windmill').nodes()

        const car_tl = new TimelineMax({ 
            repeat: -1, 
            repeatDelay: 3 
        })

        const sun_tl = new TimelineMax({ 
            repeat: -1,
            repeatDelay: 0,
            yoyo: true,
            timeScale: 1
        })

        const airballoon_tl = new TimelineMax({ 
            repeat: -1, 
            repeatDelay: 4, 
            yoyo: true 
        })

        const windmills_tl = new TimelineMax({
            repeat: -1,
            repeatDelay: 0,
            yoyo: true,
        })

        const truck_tl = new TimelineMax({
            repeat: -1,
            repeatDelay: 2,
           
        })

        car_tl
            .set(car, {
                x: 360
            })
            .to(car, 6, {
                x: -150,
                // z: 0.1, // use if jitter or shaking is really bad
                // rotationZ: 0.01, // use if jitter or shaking is really bad
                rotation: 0.01,
                ease: Power4.easeIn,
                //force3D: true
            })

        sun_tl.to(sun_rays, 30, { 
            rotation: 360, 
            transformOrigin: "50% 50%", 
            ease: Power2.easeOut 
        })

        windmills_tl.to(windmills, 15, { 
            rotation: 360, 
            transformOrigin: "50% 50%", 
            ease: Power2.easeInOut,
            delay: 0.5 
        })

        airballoon_tl
            .to(airballoon, 10, {
                x: 100,
                y: -50,
                ease: Linear.easeNone,
                force3D: true
            })
            .to(airballoon, 10, {
                x: 0,
                y: 0,
                ease: Linear.easeNone,
                force3D: true
            })

        truck_tl
            .to(truck, 2, {
                x: -30,
                z: 0.1, // use if jitter or shaking is really bad
                rotationZ: 0.01, // use if jitter or shaking is really bad
                ease: Power2.easeOut,
                rotation: 0.01,
                force3D: true
            }, 0.5)
            .to(truck, 2, {
                x: 0,
                z: 0.1, // use if jitter or shaking is really bad
                rotationZ: 0.01, // use if jitter or shaking is really bad
                ease: Power2.easeOut,
                rotation: 0.01,
                force3D: true
            })

        

        // let svg = d3.select(this.svg_element)
        // let width = +svg.attr("width")
        // let height = +svg.attr("height")

        // const view = d3.select(this.view_element)
        //     .attr("class", "view")
        //     .attr("x", 0.5)
        //     .attr("y", 0.5)
        //     .attr("width", width - 1)
        //     .attr("height", height - 1)
        //     .style("pointer-events", "all")
    }
    
}

export default Map