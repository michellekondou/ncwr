"use strict";
import { hasClass, findHighestZIndex, removeClass, addClass } from './helpers.js'
import { TweenMax, TimelineMax, TimelineLite } from '../../node_modules/gsap/TweenMax'
import { Draggable } from '../../node_modules/gsap/Draggable.js'
import DranAndDrop from './dndOneToOne'
import IOLazy from './iolazy'

const Slider = {
    init() {
        this.slider = document.querySelectorAll('.slider')
        this.next_button = document.querySelectorAll('.next')
        this.prev_button = document.querySelectorAll('.prev')
        this.setIndex(0)
        this.addEventListeners()
    },
    setIndex(idx) {
        this.slider_idx = idx
        console.log('this.slider_idx:', this.slider_idx)
        return this.slider_idx
    },
    resetSlider(page) {
        //set vars
        this.slider_idx = 0
        const slider = page.querySelector('.slider')
        const slider_id = slider.getAttribute('data-id')
        const slides = slider.querySelectorAll('.slide')
        const next_button = document.querySelector('.page-nav[data-id="' + slider_id + '"].next')
        const prev_button = document.querySelector('.page-nav[data-id="' + slider_id + '"].prev')
        //make first subpage visible
        Array.from(slides).filter((slide, i) => {
            slide.classList.remove('current')
            if (i === 0) slide.classList.add('current')
        })
        //reset slider buttons
        next_button.classList.remove('disabled')
        prev_button.classList.add('disabled')
    },
    addEventListeners() {
        Array.from(this.next_button).forEach(button => button.addEventListener('pointerup', ((e) => {
            const slider_parent = button.parentElement.closest('.app-screen.page')
            const slider = slider_parent.querySelector('.slider')
            this.getNewIndexAndRender('next', slider)
            
            //reset forms and games
            const dnd_quizes = slider_parent.querySelectorAll('.quiz-dnd')
            Array.from(dnd_quizes).forEach(quiz => {
                console.log(quiz)
                //DranAndDrop.resetDragAndDrop(quiz)
            })
            
        })))

        Array.from(this.prev_button).forEach(button => button.addEventListener('pointerup', ((e) => {
            const slider_parent = button.parentElement.closest('.app-screen.page')
            const slider = slider_parent.querySelector('.slider')
            this.getNewIndexAndRender('prev', slider)

            const dnd_quizes = slider_parent.querySelectorAll('.quiz-dnd')
            Array.from(dnd_quizes).forEach(quiz => {
                //DranAndDrop.resetDragAndDrop(quiz)
            })
        })))

        
    },
    getSlides(idx, slides) {
        let slides_arr = []
        slides.forEach((el, i ) => {
            //restore default classes for all slides before selecting the current slide
            //el.classList.remove('current')

            //add slides to array
            slides_arr.push(el)
        })
        return slides_arr[idx]
    },
    resetSlides(slides) {
        clearInterval(this.autoplayInterval)
        clearTimeout(this.autoplayTimeout)

        const form = slides[0].closest('.slider')

        slides.filter((el, i) => {
            el.classList.remove('current')
            el.classList.remove('prev-slide')
            el.classList.remove('next-slide')
            const prompt = el.querySelector('.checkbox-prompt')
            if (prompt) {
                prompt.classList.add('visually-hidden')
            }
            
            if(form.getAttribute('data-type') === 'page-pile') {
                el.style.zIndex = 0
                el.querySelector('figure').style.webkitTransform = 'translate3d(0, 0, 0)';
            }
            
            if(i === 0) {
                el.classList.add('current')
                if (form.getAttribute('data-type') === 'page-pile') {
                    el.style.zIndex = 1
                }
            }
        })

        this.next_button = form.querySelector('.next')
        this.prev_button = form.querySelector('.prev')

        this.next_button.classList.remove('disabled')
        this.prev_button.classList.add('disabled')

        //set the index back to the first slide
        this.slider_idx = 0

    },
    playSlides(slider) {
        // this.next_button.classList.add('disabled')
        // this.prev_button.classList.add('disabled')
        // const prompt = slider.querySelectorAll('.checkbox-prompt')
       
        // Array.from(prompt).forEach(prompt => {
        //     prompt.classList.remove('visually-hidden')
        // })
        // this.autoplayInterval = setInterval(() => {
        //     this.autoplayTimeout = setTimeout(() => {
        //         //this.getNewIndexAndRender('next', slider)
        //         //this.next_button.classList.add('disabled')
        //         //this.prev_button.classList.add('disabled')
        //     }, 1000);
            
        //     // this.next_button.classList.add('disabled')
        //     // this.prev_button.classList.add('disabled')
            
        //     if (this.slider_idx === this.length - 1) {
        //         clearInterval(this.autoplayInterval)
        //         clearTimeout(this.autoplayTimeout)
        //         const slides = document.querySelectorAll('.slide')
                
        //         setTimeout(() => {
        //             this.resetSlides(Array.from(slides))
        //             if(prompt) {
        //                 Array.from(prompt).forEach(prompt => {
        //                     prompt.classList.remove('visually-hidden')
        //                 })
        //             }
                    
        //             this.next_button.classList.remove('disabled')
        //             this.prev_button.classList.remove('disabled')
        //         }, 3000);
                
        //     }
        
        // }, 3000);
    },
    getNextIdx(idx = 0, length, direction, cycle) {
        switch (direction) {
            case 'next': return (idx + 1) % length
            case 'prev': return (idx == 0) && length - 1 || idx - 1
            default: return idx
        }
    },
    sliderFunctions(page) {
        IOLazy.addEventListeners(page)
    },
    getNewIndexAndRender(direction, slider) {
        const slider_id = slider.getAttribute('data-id')
        //slider slides
        const slides = slider.querySelectorAll('.slide')
        const length = slides.length
        //slider nav
        const next_button = document.querySelector('.page-nav[data-id="' + slider_id + '"].next')
        const prev_button = document.querySelector('.page-nav[data-id="' + slider_id + '"].prev')

        //handle cycle nav
        const options = {
            'data-cycle': slider.getAttribute('data-cycle'),
            'data-type': slider.getAttribute('data-type')
        }

        if (!options['data-cycle']) {

            if (direction === 'next' && this.slider_idx  === length - 2) {
                //reach the end
                next_button.classList.add('disabled')

            } else if (direction === 'prev' && this.slider_idx  === 1) {
                //reach the start
                prev_button.classList.add('disabled')

            } else {
                next_button.classList.remove('disabled')
                prev_button.classList.remove('disabled')
            }
        }

        this.slider_idx = this.getNextIdx(this.slider_idx, length, direction)

        let slides_arr = []
        slides.forEach((el, i) => {
            //restore default classes for all slides before selecting the current slide
            el.classList.remove('current')

            //add slides to array
            slides_arr.push(el)
        })

        let current_slide = slides_arr[this.slider_idx]
        let next_slide = slides_arr[this.slider_idx + 1]
        let prev_slide = slides_arr[this.slider_idx - 1]

        current_slide.classList.add('current')
        current_slide.querySelector('.inner-scroll').scrollTop = 0
        //progress bar links
        let current_slide_id = current_slide.getAttribute('data-id')
        const nav = document.querySelector('.app-screen__nav[data-id="' + slider_id + '"]')
        let active_link = nav.querySelector('.app-screen__nav li[data-target="' + current_slide_id +'"]')
       
        Array.from(nav.querySelectorAll('li')).forEach(link => {
            link.classList.remove('active')
        })
       
        active_link.classList.add('active')
        active_link.classList.add('visited')

        this.sliderFunctions(current_slide)
    }
}

export default Slider