"use strict"

import { hasClass } from './helpers'
import Navigation from './navigation'
import Slider from './slider'
import TweenMax, { Power4 } from 'gsap/TweenMaxBase'
import Map from './map'

const Page = {
    init() {
        this.page = document.querySelectorAll('.app-screen') 
        
        Array.from(this.page).forEach((page) => {
            
            //page elements
            this.page_close_button = page.querySelector('.close')
            
            //page events
            if (this.page_close_button) {
                this.page_close_button.addEventListener('click', (e) => {
                    console.log(e.target)
                    const el = e.target
                    if (el.getAttribute('data-target') === page.getAttribute('data-id')) {
                        this.closePage(page)
                        // Navigation.requestHomepage('/')
                        //push history state
                        Navigation.appPushState('/') 
                        this.mapStatic = document.querySelector('.map-static')

                        if (this.mapStatic) {
                            Navigation.requestHomepage('/')
                            //push history state
                            Navigation.appPushState('/') 
                        }
                    }
                })
            }
        })
    },
    openPage(page) {
        //open page
        page.classList.add('open')

        const mapContainer = document.getElementById('map-container')

        mapContainer.classList.add('overlay')
    },
    closePage(page) {
        //close page
        page.classList.remove('open')

        const mapContainer = document.getElementById('map-container')

        mapContainer.classList.remove('overlay')

        Navigation.appPushState('/') 
    }
}

export default Page