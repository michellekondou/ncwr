"use strict";
import { hasClass, getChild, removeClass, addClass } from './helpers.js'
import Slider from './slider.js';
import MultipleChoice from './multipleChoice'
const Forms = {
    init() {
        this.card_forms = document.querySelectorAll('form')
        this.form_submit_button = document.querySelectorAll('.submit')
        this.reset_buttons = document.querySelectorAll('.reset.submit-button')
        this.inputs = document.querySelectorAll('input[type="checkbox"], input[type="radio"]')
        this.checkbox_prompts = document.querySelectorAll('.checkbox-prompt')
        this.form_images = document.querySelectorAll('.slide__image img')
        
        this.submitForm()
        //reset the form on reset button click
        Array.from(this.reset_buttons).forEach(el => {
            el.addEventListener('click', (e) => {
                this.resetForm(e.target)
            })
        })
        //clear the form error message when an input is selected
        Array.from(this.inputs).forEach(el => {
            
            el.addEventListener('change', (e) => {
                console.log(e, e.target)
                let form_id = el.closest('form').getAttribute('id')
                let form_error_msg = document.getElementById(form_id + '--form-error')
                form_error_msg.innerHTML = ''
                Array.from(this.form_submit_button).filter(el => {
                    if (el.getAttribute('data-target') === form_id) {
                        el.classList.remove('disabled')
                    }
                })

                if (el.checked) {
                    //el.parentElement.querySelector('label').classList.add('selected')
                } else {
                    //el.parentElement.querySelector('label').classList.remove('selected')
                }
            })
        })

        if (this.checkbox_prompts) {
            Array.from(this.checkbox_prompts).forEach(el => {
                el.addEventListener('click', (e) => {
                    el.classList.toggle('selected')
                })
            })
        }
        
        Array.from(this.form_images).forEach(el => {
            el.addEventListener('click', (e) => {
                const input = el.closest('.slide').querySelector('input')
                const label = el.closest('.slide').querySelector('label')
                if(input.checked) {
                    input.checked = false
                    label.classList.remove('selected')
                } else {
                    input.checked = true
                    label.classList.add('selected')
                }
                
            })
        })

        const textarea = document.getElementsByTagName('textarea')

        Array.from(textarea).forEach(el => {
            const form_id = el.closest('form').getAttribute('id')
            const tx_submit_btn = document.querySelector('#' + form_id + '--submit')
            const tx_error_msg = document.querySelector('#' + form_id + '--form-error')
            
            //for IE
            el.addEventListener('onpropertychange', (e) => {
                tx_submit_btn.classList.remove('disabled')
                tx_error_msg.innerHTML = ''
            })

            el.addEventListener('input', (e) => {
                tx_submit_btn.classList.remove('disabled')
                tx_error_msg.innerHTML = ''
            })
        })

        this.formSlider()
        this.formStyles()
    },
    getFormData(form) {

        const form_id = form.getAttribute('id')

        if (document.getElementById(form_id).elements) { //https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/elements
            var elements = document.getElementById(form_id).elements; // all form elements
        }

        var fields = Object.keys(elements).map(function (k) {
            if (elements[k].name !== undefined) {
                
                return elements[k].name
                // special case for Edge's html collection
            } else if (elements[k].length > 0) {
                return elements[k].item(0).name
            }
        }).filter(function (item, pos, self) {
          
            return self.indexOf(item) == pos && item
        })

        var data = {}
        var dataArr = []

        fields.forEach((input) => {
            //if the input is not of checkbox type return a single value
            data[input] = elements[input].value
            
            Array.from(elements[input]).filter(el => {
                //if the input is of checkbox type return a comma separated string with all the checked answers
               if (el.type === "checkbox" && el.checked) {
                     dataArr.push(el.value)
                     data[input] = dataArr.join(', ')
                }
                
            })
        })
        //console.log(data)
        return data
    },
    submitForm() {
        require('formdata-polyfill')

        Array.from(this.form_submit_button).forEach(el => {

            const submit_id = el.getAttribute('id')
            const form_id = submit_id.substring(0, submit_id.indexOf('--'))
            const form_element = document.getElementById(form_id)
            const inputs = form_element.querySelectorAll('input')
            
            el.addEventListener('click', (e) => {

                let selected_input = []
                const target_id = e.target.getAttribute('data-target')
                const form_submit_button_loader = document.querySelector('#' + target_id + '--submit .loader')
                
                let form_error_msg = document.getElementById(target_id + '--form-error')

                Array.from(inputs).forEach(el => {
                    console.log(el)
                    let isChecked = el.checked
                    if (isChecked) {
                        selected_input.push(el)
                    }
                })
                //if at least one input is selected submit the form
                if (selected_input.length >= 1) {
                    let parent_form
                    let url
                    //show the loader on click
                    form_submit_button_loader.style.display = 'block'

                    selected_input.forEach(item => {
                        parent_form = item.closest('form')
                        url = parent_form.getAttribute('action') 
                    })

                    const ajaxSuccess = () => {
                        form_error_msg.innerHTML = ''
                        this.onSubmitSuccess(parent_form)
                    }

                    var form_data = new FormData(parent_form)
                    var form_values = form_data.getAll(selected_input[0].getAttribute('name'))
                    var checkboxData = []

                    if (form_values.length > 1) {
                        var checkboxValue
                        form_values.forEach(el => {
                            checkboxData.push(el)
                            checkboxValue = checkboxData.join(' | ')
                        })
                        form_data.set(selected_input[0].getAttribute('name'), checkboxValue)
                    }

                    const AJAXSubmit = (oFormElement) => {
                        if (!oFormElement.action) { return; }
                        var oReq = new XMLHttpRequest()
                        oReq.onload = ajaxSuccess
                        if (oFormElement.method.toLowerCase() === "post") {
                            oReq.open("post", url)
                            oReq.send(form_data)
                        }
                    }

                    AJAXSubmit(parent_form)

                    this.checkAnswers(parent_form)
                //if no inputs are selected
                } else if (selected_input.length == 0 && !hasClass(form_element, 'feedback_short_text')) { //
                    form_error_msg.innerHTML = 'You haven&prime;t chosen any answers!'
                    el.classList.add('disabled')
                }

                //for feedback short text textarea
                if (hasClass(form_element, 'feedback_short_text')) {
                    let form_error_msg = document.getElementById(form_id + '--form-error')
                    let form_submit_btn = document.getElementById(form_id + '--submit')
                    let form_submit_btn_loader = form_submit_btn.querySelector('.loader')
                    let form_textarea = form_element.querySelector('textarea')

                    if (form_textarea.value === '') {
                        form_error_msg.innerHTML = 'You haven&prime;t written something yet!'
                        form_submit_btn.classList.add('disabled')
                    } else {
                        const ajaxSuccess = () => {
                            form_error_msg.innerHTML = ''
                            this.onSubmitSuccess(form_element)
                        }

                        var form_data = new FormData(form_element)
                        var url = form_element.getAttribute('action')
                        //show the loader on click
                        form_submit_btn_loader.style.display = 'block' 

                        const AJAXSubmit = (oFormElement) => {
                            if (!oFormElement.action) { return; }
                            var oReq = new XMLHttpRequest()
                            oReq.onload = ajaxSuccess
                            if (oFormElement.method.toLowerCase() === "post") {
                                oReq.open("post", url)
                                oReq.send(form_data)
                            }
                        }

                        AJAXSubmit(form_element)
                    }
                }
            })
        })
    },
    onSubmitSuccess(form) {
        let form_id = form.getAttribute('id')
        const form_submit_button = document.getElementById(form_id + '--submit')
        const form_submit_button_loader = document.querySelector('#' + form_id + '--submit .loader')
        let form_thank_you_msg = document.getElementById(form_id + '--thankyou_message')
        let check_answer = form_thank_you_msg.querySelector('.check-answer')
        const form_checkbox_prompts = form.querySelectorAll('.checkbox-prompt')
        const inputs = form.querySelectorAll('input')
        //enable the button for when it is used again
        form_submit_button.classList.remove('disabled')
        //hide the loader
        form_submit_button_loader.style.display = 'none'
        //disable input to prevent resubmit
        Array.from(inputs).forEach(el => {
            el.setAttribute('disabled', true)
            if(el.checked) {
                el.parentElement.querySelector('label').classList.add('selected')
            }
        })
        //hide the submit button
        form_submit_button.classList.add('visually-hidden')
        //mark the form as submitted
        form.classList.add('submitted')
        //display a thank you message
        form_thank_you_msg.style.display = 'flex'
        //display an input message
        if (check_answer) {
            TweenMax.set(check_answer, {
                opacity: 1
            })
        }

        if (hasClass(form, 'slider')) {
            const slides = document.querySelectorAll('.slide')
            Slider.resetSlides(Array.from(slides))

            Slider.playSlides(form)
        }
        if (form_checkbox_prompts) {
            Array.from(form_checkbox_prompts).forEach(el => {
                el.classList.remove('visually-hidden')
            })
        }
        //add class to checked input
        //selectedOption.siblings('label').addClass('selected')
        //radio input: check correct answers to display appropriate msg
    },
    checkAnswers(form) {
        let form_id = form.getAttribute('id')
        let form_thank_you_msg = document.getElementById(form_id + '--thankyou_message')
        let check_answer = form_thank_you_msg.querySelector('.check-answer')
        const reset_button = document.querySelector('#' + form_id + "--reset")
        const correct_answers = []
        const selected_answers = []
        const inputs = form.querySelectorAll('input')

        if (form_thank_you_msg.contains(check_answer)) {

            Array.from(inputs).forEach(el => {
                
                if (el.type === 'checkbox' || hasClass(el, 'radio-multiple')) {

                    if (el.getAttribute('data-type') === 'correct') {
                        correct_answers.push(el)
                    }
                    if (el.checked) {
                        selected_answers.push(el)
                    }

                    //only applies to checkbox inputs where many may be selected
                    const is_same = (correct_answers.length == selected_answers.length) && correct_answers.every(function (element, index) {
                        return element === selected_answers[index];
                    })

                    if (is_same) {
                        check_answer.classList.remove('wrong')
                        check_answer.classList.add('correct')
                        check_answer.innerHTML = 'Well done! All answers correct!'
                    } else {
                        check_answer.classList.remove('correct')
                        check_answer.classList.add('wrong')
                        check_answer.innerHTML = 'There were some mistakes!'

                        setTimeout(() => {
                            reset_button.classList.remove('visually-hidden')
                            reset_button.classList.add('selected')
                        }, 500)

                        setTimeout(() => {
                            TweenMax.to(check_answer, 0.6, {
                                opacity: 0
                            })
                        }, 4000)
                    }

                } else if (el.type === 'radio' && el.checked) {
                    
                    if (el.getAttribute('data-type') === 'correct') {
                        check_answer.classList.add('correct')
                        check_answer.innerHTML = 'Correct!'
                    } else if (el.getAttribute('data-type') === 'wrong') {
                        check_answer.classList.add('wrong')
                        check_answer.innerHTML = 'Wrong!'

                        setTimeout(() => {
                            reset_button.classList.remove('visually-hidden')
                            reset_button.classList.add('selected')
                        }, 500)
                    }
                }
            })  
        }

    },
    resetForm(handle){
        let form_id = handle.getAttribute('data-target')
        const reset_button = handle
        const submit_button = document.querySelector('#' + form_id + "--submit")
        const thankyou_message = document.querySelector('#' + form_id + "--thankyou_message")
        const reset_button_loader = reset_button.querySelector('.loader')
        const form = document.getElementById(form_id)
        
        const inputs = form.querySelectorAll('input')
        const url = form.getAttribute('action')
        let selected_input

        reset_button_loader.style.display = 'flex'

        Array.from(inputs).forEach(el => {
            selected_input = el.getAttribute('name')
        })

        const ajaxSuccess = () => {
            form.classList.remove('submitted')    
            const inputs = form.querySelectorAll('input[type="text"], input[type="password"], input[type="file"], select, textarea')
            Array.from(inputs).forEach(el => {
                el.value = ''
            })
            const quiz_inputs = form.querySelectorAll('input[type="radio"], input[type="checkbox"]')
            Array.from(quiz_inputs).forEach(el => {
                el.removeAttribute('checked')
                el.removeAttribute('disabled')

                //el.classList.remove('selected')
            })
            
            const quiz_prompts = form.querySelectorAll('.checkbox-prompt')
            Array.from(quiz_prompts).forEach(el => {
                el.classList.add('visually-hidden')
            })

            const labels = form.querySelectorAll('label')
            Array.from(labels).forEach(el => {
                el.classList.remove('selected')
            })

            handle.classList.add('visually-hidden')
            reset_button_loader.style.display = 'none'
            thankyou_message.style.display = 'none'

            Array.from(thankyou_message.querySelectorAll('.check-answer')).forEach(el => {
                el.classList.remove('correct')
                el.classList.remove('wrong')
            })

            submit_button.classList.remove('visually-hidden')

            if (hasClass(form, 'slider')) {
                submit_button.classList.add('visually-hidden')
            }

            form.reset()
            //reset slider to beginning
            if(hasClass(form, 'slider')) {
                const slides = document.querySelectorAll('.slide')
                Slider.resetSlides(Array.from(slides))
            }
        }

        const AJAXSubmit = (oFormElement) => {
            if (!oFormElement.action) { return; }
            var oReq = new XMLHttpRequest()
            var formData = new FormData(form)
            formData.set(selected_input, 'Form cleared')
            if (oFormElement.method.toLowerCase() === "post") {
                oReq.open("post", url)
                oReq.send(formData)
            }
            oReq.onload = ajaxSuccess
        }


        AJAXSubmit(form)
    },
    formSlider() {
        
        this.slide = document.querySelectorAll('form.checkbox .checkbox')
        Array.from(this.card_forms).forEach((el, index) => {
            
            Array.from(this.slide).forEach((slide, i) => {
                
                const inputs = slide.querySelectorAll('input[type="checkbox"], input[type="radio"]')

                if (i === 0) {
                    slide.classList.add('current')
                }
            })

        })
       
    },
    formStyles() {
        this.checkbox_form = document.querySelectorAll('form.checkbox')
        this.submit_button = document.querySelectorAll('.submit-button')

        Array.from(this.submit_button).forEach(button => {
            if(!hasClass(button, 'visually-hidden')) {
                button.setAttribute('tabindex', 0)
            }
        })
        // if (this.checkbox_form.length) {
        //     Array.from(this.checkbox_form).forEach(el => {
        //         this.checkbox_legend = el.querySelector('legend')
        //         if(this.checkbox_legend) {
        //             this.checkbox_slide = el.querySelectorAll('.slide') 
        //             Array.from(this.checkbox_slide).forEach(el => {
        //                 el.style.top = this.checkbox_legend.offsetHeight +'px'
        //             })
        //         }
        //     })
        // }
        
        
    }
}

export default Forms



//https://codereview.stackexchange.com/questions/132397/prev-next-buttons-for-a-circular-list