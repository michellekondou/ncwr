//helpers
const hasClass = (el, cls) => {
    return (' ' + el.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

//
const getChild = (el, cls) => {
    return el.querySelector(cls)
}

//
const removeClass = (el, cls) => {
    return el.classList.remove(cls)
}

//
const addClass = (el, cls) => {
    return el.classList.add(cls)
}

//
/**
 * Helper function to figure out higher z-index
 */
const findHighestZIndex = (el) => {
    const nodes = document.querySelectorAll(el);
    let highest = 0;
    for (let i = 0; i < nodes.length; i++) {
        let zindex = document.defaultView.getComputedStyle(nodes[i], null).getPropertyValue("z-index");
        if ((zindex > highest) && (zindex != 'auto')) {
            highest = zindex;
        }
    }
    console.log(highest)
    return highest;
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
const shuffle = (a) => {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    
    //return a
}



export { hasClass, getChild, removeClass, addClass, findHighestZIndex, shuffle }