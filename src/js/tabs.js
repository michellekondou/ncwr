
"use strict"

const Tabs = {
    init(options) {
        const el = document.querySelector(options.el)
        const tabNavigationLinks = el.querySelectorAll(options.tabNavigationLinks)
        const tabContentContainers = el.querySelectorAll(options.tabContentContainers)
        const activeIndex = 0
        const initCalled = false
        const link

        //     /**
//      * tabs
//      *
//      * @description The Tabs component.
//      * @param {Object} options The options hash
//      */
//     var tabs = function (options) {

//         var el = document.querySelector(options.el);
//         var tabNavigationLinks = el.querySelectorAll(options.tabNavigationLinks);
//         var tabContentContainers = el.querySelectorAll(options.tabContentContainers);
//         var activeIndex = 0;
//         var initCalled = false;
//         var link;

//         /**
//          * init
//          *
//          * @description Initializes the component by removing the no-js class from
//          *   the component, and attaching event listeners to each of the nav items.
//          *   Returns nothing.
//          */
//         var init = function () {
//             if (!initCalled) {
//                 initCalled = true;
//                 el.classList.remove('no-js');
//                 for (var i = 0; i < tabNavigationLinks.length; i++) {
//                     link = tabNavigationLinks[i];
//                     handleClick(link, i);
//                 }
//             }
//         };

//         /**
//          * handleClick
//          *
//          * @description Handles click event listeners on each of the links in the
//          *   tab navigation. Returns nothing.
//          * @param {HTMLElement} link The link to listen for events on
//          * @param {Number} index The index of that link
//          */
//         var handleClick = function (link, index) {
//             link.addEventListener('click', function (e) {
//                 e.preventDefault();
//                 goToTab(index);
//             }, { passive: true });
//         };

//         /**
//          * goToTab
//          *
//          * @description Goes to a specific tab based on index. Returns nothing.
//          * @param {Number} index The index of the tab to go to
//          */
//         var goToTab = function (index) {
//             if (index !== activeIndex && index >= 0 && index <= tabNavigationLinks.length) {
//                 tabNavigationLinks[activeIndex].classList.remove('is-active');
//                 tabNavigationLinks[index].classList.add('is-active');
//                 tabContentContainers[activeIndex].classList.remove('is-active');
//                 tabContentContainers[index].classList.add('is-active');
//                 activeIndex = index;
//             }
//         };

//         /**
//          * Returns init and goToTab
//          */
//         return {
//             init: init,
//             goToTab: goToTab
//         };

//     };

//     /**
//      * Attach to global namespace
//      */
//     window.tabs = tabs;
    }
}

export default Tabs

/*------------------------------------*\
  #TAB NAVIGATION SYMBOLS
\*------------------------------------*/
/**
 * Switch symbol size via classname depending on viewport size
 */
// function switchIconSize() {
//     var icon = $('.symbol-container .icon');
//     var icon_class = icon.attr('class');
//     var re = / *\bsymbol\b/g;
//     var reNot = / *\bsymbol-small\b/g;
//     //look for the class prefix symbol-small in icon class-names
//     var testRe = reNot.exec(icon_class);
//     for (var is = 0; is < icon.length; is++) {
//         _is = icon[is];
//         if (document.documentElement.clientHeight <= 768 && testRe === null) {
//             _is.className = _is.className.replace(/ *\bsymbol\b/g, " symbol-small");
//         } else if (document.documentElement.clientHeight > 768 && testRe !== null) {
//             _is.className = _is.className.replace(/ *\bsymbol-small\b/g, " symbol");
//         }
//     }
// }

// switchIconSize();

// $(window).resize(function () {
//     location.reload();
//     var icon = $('.symbol-container .icon');
//     var icon_class = icon.attr('class');
//     var re = / *\bsymbol\b/g;
//     var reNot = / *\bsymbol-small\b/g;
//     //look for the class prefix symbol-small in icon class-names
//     var testRe = reNot.exec(icon_class);
//     if (document.documentElement.clientHeight <= 768 && testRe === null) {
//         for (var is = 0; is < icon.length; is++) {
//             _is = icon[is];
//             _is.className = _is.className.replace(/ *\bsymbol\b/g, " symbol-small");
//         }
//     }
//     if (document.documentElement.clientHeight > 768) {
//         for (is = 0; is < icon.length; is++) {
//             _is = icon[is];
//             _is.className = _is.className.replace(/ *\bsymbol-small\b/g, " symbol");
//         }
//     }

// });

// (function () {

//     'use strict';

//     /**
//      * tabs
//      *
//      * @description The Tabs component.
//      * @param {Object} options The options hash
//      */
//     var tabs = function (options) {

//         var el = document.querySelector(options.el);
//         var tabNavigationLinks = el.querySelectorAll(options.tabNavigationLinks);
//         var tabContentContainers = el.querySelectorAll(options.tabContentContainers);
//         var activeIndex = 0;
//         var initCalled = false;
//         var link;

//         /**
//          * init
//          *
//          * @description Initializes the component by removing the no-js class from
//          *   the component, and attaching event listeners to each of the nav items.
//          *   Returns nothing.
//          */
//         var init = function () {
//             if (!initCalled) {
//                 initCalled = true;
//                 el.classList.remove('no-js');
//                 for (var i = 0; i < tabNavigationLinks.length; i++) {
//                     link = tabNavigationLinks[i];
//                     handleClick(link, i);
//                 }
//             }
//         };

//         /**
//          * handleClick
//          *
//          * @description Handles click event listeners on each of the links in the
//          *   tab navigation. Returns nothing.
//          * @param {HTMLElement} link The link to listen for events on
//          * @param {Number} index The index of that link
//          */
//         var handleClick = function (link, index) {
//             link.addEventListener('click', function (e) {
//                 e.preventDefault();
//                 goToTab(index);
//             }, { passive: true });
//         };

//         /**
//          * goToTab
//          *
//          * @description Goes to a specific tab based on index. Returns nothing.
//          * @param {Number} index The index of the tab to go to
//          */
//         var goToTab = function (index) {
//             if (index !== activeIndex && index >= 0 && index <= tabNavigationLinks.length) {
//                 tabNavigationLinks[activeIndex].classList.remove('is-active');
//                 tabNavigationLinks[index].classList.add('is-active');
//                 tabContentContainers[activeIndex].classList.remove('is-active');
//                 tabContentContainers[index].classList.add('is-active');
//                 activeIndex = index;
//             }
//         };

//         /**
//          * Returns init and goToTab
//          */
//         return {
//             init: init,
//             goToTab: goToTab
//         };

//     };

//     /**
//      * Attach to global namespace
//      */
//     window.tabs = tabs;

// })();

// var myTabs = tabs({
//     el: '#tabs',
//     tabNavigationLinks: '.c-tabs-nav__link',
//     tabContentContainers: '.c-tab'
// });

// myTabs.init();

// };