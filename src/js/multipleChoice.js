"use strict";

import { hasClass, shuffle } from './helpers.js'

const MultipleChoice = {
    init() {
        this.multiple_choice_grid = document.querySelectorAll('.multiple-choice--grid')

        Array.from(this.multiple_choice_grid).forEach((el, i) => {
            const radio_pair = el.querySelectorAll('.radio-block')
            const target_items = []
            target_items.push(radio_pair)
            target_items.forEach(el => {
                if (el && el[0]) {
                    this.shuffleTargets(el, el[0].parentElement)
                }
                
            })
        })
    },
    shuffleTargets(targets, parent) {
        if (targets) {
            //set an array to save the shuffled items
            let arr = []
            //populate the array with the targets
            targets.forEach(el => {
                arr.push(el.outerHTML)
            })
            //shuffle the array
            shuffle(arr)
            //repopulate the target container with the shuffled items
            parent.innerHTML = arr.join('')
        }
    },
}

export default MultipleChoice