"use strict"

import Forms from './forms'
import Slider from './slider'
import Map from './map'
import Navigation from './navigation'
import Page from './page'

const Preloader = {
    init() {
        window.addEventListener('load', (e) => {
            Map.init()
            console.log('loaded:')
            setTimeout(() => {
                document.getElementById('preloader').classList.add('visually-hidden')
                // Forms.init()
            
             
            }, 3000)
            
         }, { passive: true });
    }
}

export default Preloader

// $('#preloader h2').removeClass('visually-hidden');
// var tl = new TimelineLite({
//     onStart: function () {
//         $('#preloader h2').removeClass('visually-hidden');
//         $('.logo, .floating-element--button, .map-controls').removeClass('visually-hidden');
//     },
//     onComplete: function () {
//         $('#preloader').addClass('visually-hidden');
//         $('.logo, .floating-element--button, .map-controls, .info-panel').removeClass('visually-hidden');
//         console.log('content loaded');
//     }