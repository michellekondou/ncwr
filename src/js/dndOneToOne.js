"use strict"
import { hasClass, findHighestZIndex, removeClass, addClass, shuffle } from './helpers.js'
import { TweenMax, TimelineMax, TimelineLite } from '../../node_modules/gsap/TweenMax'
import { Draggable } from '../../node_modules/gsap/Draggable.js'

const DranAndDrop = {
    init(page) {
        this.dnd_quiz = page.querySelectorAll('.quiz-dnd')
        this.draggables = []

        Array.from(this.dnd_quiz).forEach((el, i) => {
            const quiz = el
            const quiz_id = quiz.getAttribute('id')
            const dnd_reset = quiz.parentElement.querySelector('#' + quiz_id + '--reset')
            const targets = el.querySelectorAll('.dnd-target')
            const origins = el.querySelectorAll('.dnd-origin')
           
            const target_container = el.querySelector('.dnd-target-container')
            const target_items = []
            const origin_items = []

            target_items.push(targets)
            origin_items.push(origins)

            Array.from(origins).filter((origin, i) => {
                //if (i === 0) origin.querySelector('.drag-handle').classList.add('glow-infinite')
            })
            this.dragElements(el, i)
            
            target_items.forEach(el => {
                this.shuffleTargets(el, target_container)
            })

            this.checkAnswers(quiz)
            this.showCorrectAnswers(quiz)

            dnd_reset.addEventListener('pointerdown', (e) => {
                this.resetDragAndDrop(quiz)
            })

            document.addEventListener('play', function (e) {
                const sounds = el.querySelectorAll('audio.audio-player')
            
                Array.from(sounds).forEach(audio => {
                    if (audio != e.target) {
                        audio.pause()
                    }
                })
            }, true)

            // document.addEventListener('play', function (e) {
            //     const audios = document.getElementsByTagName('audio')
            //     for (var i = 0, len = audios.length; i < len; i++) {
            //         if (audios[i] != e.target) {
            //             audios[i].pause()
            //         }
            //     }
            // }, true)

            // Array.from(sounds).forEach(sound => {
            //     console.log(sound)
            //     sound.addEventListener("load", function () {
            //         console.log('The file is loaded!');
            //     }, false);
            //     sound.addEventListener("canplaythrough", function () {
            //         console.log('The file is loaded and ready to play!');
            //     }, false);
            //     sound.addEventListener("play", function () {
            //         console.log('play!');
            //     }, false);
            // })

            
        })
    },
    shuffleTargets(targets, parent) {
        if (targets) {
            //set an array to save the shuffled items
            let arr = []
            //populate the array with the targets
            targets.forEach(el => {
                arr.push(el.outerHTML)
            })
            //shuffle the array
            shuffle(arr)
            //repopulate the target container with the shuffled items
            parent.innerHTML = arr.join('')
        }
    },
    draggableSettings(quiz) {
        this.quiz_id = quiz.getAttribute('id')
        this.dnd_show_correct = document.querySelector('#' + this.quiz_id + '--show-correct-answers')
        this.dnd_check = document.querySelector('#' + this.quiz_id + '--check-answers')
        this.dnd_reset = document.querySelector('#' + this.quiz_id + '--reset')
        this.dnd_prompt = document.querySelector('#' + this.quiz_id + '--prompt')
        this.dnd_targets = document.querySelectorAll('#' + this.quiz_id + ' .drag-handle-target')
    },
    dragElements(quiz, index) {
        index = index + 1 //drag and drop counter starts at 1
        const quiz_id = quiz.querySelector('.questions').getAttribute('data-id')
        //make all the targets available on quiz load
        Array.from(document.querySelectorAll('#' + quiz_id + ' .dnd-target')).forEach(el => {
            el.classList.add('available')
        })

        const draggables = Draggable.create('#' + quiz_id + ' .drag-handle', {
            type: "x,y",
            bounds: ('[data-id="' + quiz_id +'"]'),
            allowNativeTouchScrolling: false,
            onPressInit: function () {
              
                //set variables to use later
                this.dnd_overlapThreshold = "90%";
                this.dnd_show_correct = document.querySelector('#' + quiz_id + '--show-correct-answers')
                this.dnd_check = document.querySelector('#' + quiz_id + '--check-answers')
                this.dnd_reset = document.querySelector('#' + quiz_id + '--reset')
                this.dnd_prompt = document.querySelector('#' + quiz_id + '--prompt')
                this.dnd_target_container = this.target.closest('.quiz-dnd').querySelector('.dnd-target-container')
                this.dnd_origin_container = this.target.closest('.quiz-dnd').querySelector('.dnd-origin-container')

                // record the starting values so we can compare them later...
                // for each target find its position in the bounds container and assign it to the 
                // origin, doing this here because hidden elements do not have offsets, needs to be
                // retriggered
                this.dnd_targets = this.dnd_target_container.querySelectorAll('.dnd-target')
                this.dnd_available_targets = this.dnd_target_container.querySelectorAll('.available')
                this.positioned = this.dnd_origin_container.querySelectorAll('.positioned')
                this.target_x_in_drop_area

                Array.from(this.dnd_targets).forEach(target => {
                    const target_parent = target.parentElement
                    //get the target handle position relative to its parent li element
                    const dnd_target_x = target.offsetLeft
                    const dnd_target_y = target.offsetTop

                    //get the parent li element position relative to its ul
                    const dnd_parent_x = target_parent.offsetLeft
                    const dnd_parent_y = target_parent.offsetTop

                    //subtract target handle position from li to find exact handle target position in bounds container
                    const data_x = dnd_parent_x - dnd_target_x
                    const data_y = dnd_parent_y - dnd_target_y

                    //assign the position as attributes to grab later in dragEnd
                    target.setAttribute('data_x', data_x)
                    target.setAttribute('data_y', data_y) 
                })

                //highlight the origin text while dragging
                Array.from(document.querySelectorAll('.drag-handle')).forEach(el => {
                    const origin = this.dnd_origin_container.querySelector('[data-identifier="' + this.target.getAttribute('data-identifier') + '"]')
                    origin.parentElement.classList.add('highlighted')
                    el.classList.remove('glow-infinite')
                })

                this.target.classList.add('static-glow')

            },
            onDragStart: function () {
                //show draggable clones
                Array.from(this.dnd_origin_container.querySelectorAll('.clone')).forEach(el => {
                    el.classList.remove('visually-hidden')
                })
                //if drag starts while in drop area just return point to position
                Array.from(this.dnd_targets).forEach(el => {
                    el.classList.remove('correct')
                    el.classList.remove('wrong')
                    if(el.getAttribute('data-hit') !== '') {
                        if (this.hitTest(el, this.dnd_overlapThreshold)) {
                            el.classList.add('available')
                            el.classList.remove('showOver')
                            el.classList.remove('correct')
                            el.classList.remove('wrong')
                        }
                    }
                })
                //hide reset button
                this.dnd_show_correct.classList.add('visually-hidden')

                this.dnd_check.classList.remove('visually-hidden')
                //clear the prompt message box
                this.dnd_prompt.innerHTML = ''

                //remove correct and wrong highlight classes from targets
                Array.from(this.positioned).forEach(el => {
                    el.classList.remove('highlight-correct')
                    el.classList.remove('highlight-wrong')
                })

                //highlight the origin text while dragging
                Array.from(this.target).forEach(el => {
                    const origin = this.dnd_origin_container.querySelector('[data-identifier="' + this.target.getAttribute('data-identifier') + '"]')
                    origin.parentElement.classList.add('highlighted')
                })

            },
            onDrag: function () {
                Array.from(this.dnd_targets).forEach(target => {
                    if (this.hitTest(target, this.dnd_overlapThreshold)) {
                        //if draggable in target range highlight it and animate it
                        target.classList.add('showOver')
                        target.classList.add('ripple')
                    } else {
                        target.classList.remove('showOver')
                        target.classList.remove('ripple')
                        
                        if (target.getAttribute('data-hit') === this.target.getAttribute('data-identifier')) {
                            //on drag start check the data-hit attribute - if it has a value it is because a draggable instance has been assigned to it and passed it its data-identifier attribute
                            //if it has a value it means draggable was there
                            //remove the data-hit attribute value and signify that it is available again
                            target.removeAttribute('data-hit')
                            target.classList.add('available')
                        }
                    }
                })
            },
            onDragEnd: function (e) {
                let snapMade = false

                Array.from(this.dnd_targets).forEach(el => {
                    const current_dnd_target = el
                    const identifier = current_dnd_target.getAttribute('data-identifier')

                    if (this.hitTest(current_dnd_target, this.dnd_overlapThreshold)) {
                        snapMade = true
                        //connect source and target via an identifier
                        //give the draggable item an attr of data-target with a value of the hit id
                        //also give it a couple of styling classes
                        this.target.setAttribute('data-target', identifier)
                        this.target.classList.add("positioned")
                        this.target.classList.add("ripple")
                        //after a hit has been made reveal the check hotspot button
                        this.dnd_check.classList.remove('disabled')
                        this.dnd_check.classList.remove('visually-hidden')
                        //if the item has been positioned correctly, assign styling classes
                        if (this.target.getAttribute('data-target') === this.target.getAttribute('data-identifier')) {
                            this.target.classList.remove("wrong")
                            this.target.classList.remove("highlight-wrong")
                            this.target.classList.remove("correct")
                            this.target.classList.remove("highlight-correct")
                            this.target.classList.add("correct")
                        } else if (this.target.getAttribute('data-target') !== this.target.getAttribute('data-identifier')) {
                            this.target.classList.remove("wrong")
                            this.target.classList.remove("highlight-wrong")
                            this.target.classList.remove("correct")
                            this.target.classList.remove("highlight-correct")
                            this.target.classList.add("wrong")
                        }

                        if (hasClass(current_dnd_target, 'available')) {
                            //if there isn't one there already
                            //give the target an attribute of data-hit to mark that there has been a match
                            current_dnd_target.setAttribute('data-hit', this.target.getAttribute('data-identifier'))
                            //move item to position if position available and overlapThreshold condition is met
                            const tl = new TimelineLite()
                            
                            this.drop_area_y = current_dnd_target.getBoundingClientRect().top
                            this.target_y = this.target.getBoundingClientRect().top
                             tl
                                .to(this.target, 0.5, {
                                    x: 114,
                                    y: current_dnd_target.offsetTop + this.minY + 8 //8 is padding top
                                })
                        } else {
                            //if the position isn't available (does not have an available class) send it back to its starting position)
                            this.dnd_prompt.classList.remove('correct')
                            this.dnd_prompt.classList.add('wrong')
                            this.dnd_prompt.innerHTML = 'That position is not available, try a different one!'

                            this.target.classList.remove("positioned")
                            this.target.classList.remove("wrong")
                            this.target.classList.remove("highlight-wrong")
                            this.target.classList.remove("correct")
                            this.target.classList.remove("highlight-correct")

                            //show a message that an item has already been placed there and return item to starting position
                            const tl_return = new TimelineLite()
                            //if (hasClass(this.target, 'positioned')) {}
                            tl_return
                                .to(this.target, 0.1, {
                                    opacity: 1,
                                    x: 0,
                                    y: 0,
                                    rotation: 0.01//firefox efing bug
                                })
                        }
                        //if there is a current target (i.e. a match has been made, don't allow it to be a target as long as its populated with another draggable item)
                        current_dnd_target.classList.remove('available')
                    } 
                })

                if (!snapMade) {
                    this.target.classList.remove("positioned")
                    this.target.classList.remove("wrong")
                    this.target.classList.remove("highlight-wrong")
                    this.target.classList.remove("correct")
                    this.target.classList.remove("highlight-correct")
                    this.target.classList.remove("showOver")
                    this.target.classList.remove("ripple")

                    this.positioned = this.dnd_origin_container.querySelectorAll('.positioned')

                    if (this.positioned.length === 0) {
                        //only show the check hotspot button if at least an item has been dragged
                        this.dnd_check.classList.add('disabled')
                    } else if (this.positioned.length !== 0) {
                        this.dnd_check.classList.remove('disabled')
                    }

                    this.dnd_reset.classList.add('visually-hidden')
                    this.target.setAttribute('data-target', '')
                    TweenLite.to(this.target, 0.2, {
                        css: {
                            x: 0,
                            y: 0,
                            rotation: 0.01//firefox efing bug
                        }
                    })

                    //find the target with the same data-target attribute as this.target
                    const drag_handle_targets = document.querySelectorAll('.drag-handle-target')
                    Array.from(drag_handle_targets).forEach(el => {
                        el.classList.remove('showOver')
                    })
                }
                //when hovering over a positioned element highlight the origin text
                Array.from(document.querySelectorAll('.drag-handle.positioned')).forEach(el => {
                    const origin = this.dnd_origin_container.querySelector('[data-identifier="' + el.getAttribute('data-identifier') + '"]')
                    el.addEventListener('mouseover', (e) => {
                        origin.parentElement.classList.add('highlighted')
                    })

                    el.addEventListener('mouseout', (e) => {
                        origin.parentElement.classList.remove('highlighted')
                    })
                })

            },
            onRelease: function (e) {
                //unhighlight the origin text after 1 1/2 a second
                Array.from(document.querySelectorAll('.drag-handle')).forEach(el => {
                    const origin = this.dnd_origin_container.querySelector('[data-identifier="' + this.target.getAttribute('data-identifier') + '"]')
                    origin.parentElement.classList.remove('highlighted')
                })

                this.target.classList.remove('static-glow')
            }
        })
        
        draggables.index = index
    },
    checkAnswers(quiz) {
        const quiz_id = quiz.getAttribute('id')
        const dnd_check = document.querySelector('#' + quiz_id + '--check-answers')
        const dnd_show_correct = document.querySelector('#' + quiz_id + '--show-correct-answers')
        const dnd_prompt = document.querySelector('#' + quiz_id + '--prompt')
        const dnd_reset = document.querySelector('#' + quiz_id + '--reset')

        dnd_check.addEventListener('pointerup', (e) => {

            const dnd_check_id = e.target.getAttribute('id')
            const dnd_id = dnd_check_id.substring(0, dnd_check_id.indexOf('--'))
            const draggable_item = document.querySelectorAll('#' + dnd_id + ' .drag-handle')
            const draggable_target = document.querySelectorAll('#' + dnd_id + ' .drag-handle-target')
            const correct = document.querySelectorAll('#' + dnd_id + ' .drag-handle.correct')
            const wrong = document.querySelectorAll('#' + dnd_id + ' .drag-handle.wrong')

            Array.from(correct).forEach(el => {
                el.classList.add('highlight-correct')
                //el.parentElement.closest('.dnd-target').add('correct')
                let parent = document.querySelector('.dnd-target[data-identifier="' + el.getAttribute('data-target') +'"]')
                if (parent) {
                    parent.classList.add('correct')
                }
            })

            Array.from(wrong).forEach(el => {
                el.classList.add('highlight-wrong')
                let parent = document.querySelector('.dnd-target[data-identifier="' + el.getAttribute('data-target') + '"]')
                if (parent) {
                    parent.classList.add('wrong')
                }
            })

            Array.from(draggable_target).forEach(el => {
                el.classList.remove('showOver')
                el.classList.remove('ripple')
            })

            const positioned = document.querySelectorAll('#' + dnd_id + ' .positioned')
            const correct_positioned = document.querySelectorAll('#' + dnd_id + ' .positioned.correct')

            if (draggable_item.length === correct_positioned.length) {
                dnd_reset.classList.remove('visually-hidden')
                dnd_check.classList.add('visually-hidden')

                dnd_prompt.classList.remove('wrong')
                dnd_prompt.classList.add('correct')
                dnd_prompt.innerHTML = 'Well done! All answers correct!'

                Array.from(draggable_item).forEach(el => {
                    el.classList.add('disabled')
                })

            } else if (draggable_item.length === positioned.length && positioned.length !== correct_positioned.length) {
                setTimeout(() => {
                    dnd_check.classList.add('visually-hidden')
                    dnd_show_correct.classList.remove('visually-hidden')
                }, 100)
            }
        })
    },
    showCorrectAnswers(quiz) {

        const quiz_id = quiz.getAttribute('id')
        const dnd_check = document.querySelector('#' + quiz_id + '--check-answers')
        const dnd_show_correct = document.querySelector('#' + quiz_id + '--show-correct-answers')
        const dnd_reset = document.querySelector('#' + quiz_id + '--reset')

        dnd_show_correct.addEventListener('pointerdown', (e) => {

            const dnd_check_id = e.target.getAttribute('id')
            const dnd_id = dnd_check_id.substring(0, dnd_check_id.indexOf('--'))
            const right_positions = document.querySelectorAll('#' + dnd_id + ' .right-positions')
            const draggable_item = document.querySelectorAll('#' + dnd_id + ' .drag-handle')
            const draggable_target = document.querySelectorAll('#' + dnd_id + ' .drag-handle-target')
            const clones = document.querySelectorAll('#' + dnd_id + ' .clone')

            var ts = new TimelineMax()
            ts
                .set(draggable_item, {
                    zIndex: -1, //setting display to none so its not draggable while the show correct answer animation
                    x: 0,
                    y: 0,
                    rotation: 0.01//firefox efing bug 
                }).set(draggable_target, {
                    opacity: 0
                })

            Array.from(clones).forEach(el => {
                el.classList.remove('default')
            })

            Array.from(draggable_item).forEach(el => {
                let parent = document.querySelector('.dnd-target[data-identifier="' + el.getAttribute('data-target') + '"]')
               
                parent.classList.remove('correct')
                parent.classList.remove('wrong')
                el.classList.remove('positioned')
                el.classList.remove('highlight-correct')
                el.classList.remove('highlight-wrong')
                el.classList.remove('wrong')
                el.classList.remove('correct')
                el.classList.add('disabled')
            })

            var dnd_correct_answers = []
            var dnd_correct_answer_parent = []

            Array.from(right_positions).forEach(el => {
                el.classList.remove('no-opacity')
                dnd_correct_answers.push(el)
                var el_parent = el.parentElement.closest('.dnd-target')
                dnd_correct_answer_parent.push(el_parent)
            })

            
            var tl = new TimelineMax()
            tl.staggerFromTo(dnd_correct_answers, 0.6,
                {
                    css: {
                        'opacity': 0,
                    },
                    
                },
                {
                    css: {
                        'opacity': 1
                    },
                    onStart: function (tween) {
                        tween.target.parentElement.closest('.dnd-target').classList.add('correct')
                    },
                    onStartParams: ["{self}"],
                },
                
                0.3)

            tl.staggerTo(dnd_correct_answer_parent, 0.6,
                {
                    borderColor: 'green'
                },
                0.3)

            dnd_reset.classList.remove('visually-hidden')
            dnd_show_correct.classList.add('visually-hidden')
            dnd_show_correct.classList.remove('disabled')

            
        })
    },
    resetDragAndDrop(quiz) {

        if (quiz) {
            const quiz_id = quiz.getAttribute('id')
            const right_positions = quiz.querySelectorAll('.right-positions')
            const draggable_items = quiz.querySelectorAll('.drag-handle')
            const draggable_targets = quiz.querySelectorAll('.dnd-target')
            const target_container = quiz.querySelector('.dnd-target-container')
            const dnd_show_correct = document.querySelector('#' + quiz_id + '--show-correct-answers')
            const dnd_check = document.querySelector('#' + quiz_id + '--check-answers')
            const dnd_reset = document.querySelector('#' + quiz_id + '--reset')
            const prompt = document.querySelector('#' + quiz_id + '--prompt')

            prompt.classList.add('default')
            prompt.classList.remove('correct')
            prompt.innerHTML = ''

            Array.from(right_positions).forEach(el => {
                el.removeAttribute('style')
                el.classList.add('no-opacity')
            })

            Array.from(draggable_items).forEach((el, i) => {
                el.classList.remove('positioned')
                el.classList.remove('correct')
                el.classList.remove('wrong')
                el.classList.remove('highlight-wrong')
                el.classList.remove('highlight-correct')
                el.classList.remove('ripple')
                el.classList.remove('disabled')
                if (i === 0) {
                    el.classList.add('glow-infinite')
                }
            })

            Array.from(draggable_targets).forEach(el => {
                el.classList.remove('showOver')
                el.classList.remove('ripple')
                el.classList.remove('correct')
                el.classList.remove('wrong')
                el.classList.add('available')
                el.setAttribute('data-hit', '')
                el.setAttribute('style', '')
                el.querySelector('.drag-handle-target').style.opacity = 1
            })

            const tl = new TimelineLite()
            tl
                .set(draggable_items, {
                    zIndex: 1,
                    opacity: 1,
                    x: 0,
                    y: 0,
                    rotation: 0.01//firefox efing bug
                })

            setTimeout(() => {
                dnd_show_correct.classList.add('visually-hidden')
                dnd_check.classList.add('disabled')
                dnd_check.classList.remove('visually-hidden')
                dnd_reset.classList.add('visually-hidden')
            }, 100)


            this.shuffleTargets(draggable_targets, target_container)
            //kill everything
            TweenMax.killAll()

        }
    },
    audioFiles() {
        this.sound_element
        this.sound_src
        this.audio_el
        this.audios

        // sound_element = $('<audio/>', {
        //     "preload": "auto",
        //     "controls": true,
        //     'class': 'audio-element'
        // }).appendTo($(parent.dnd_target_sound_el));

        // //loop through the sound element and grab the src to append to audio
        // for (var se = 0; se < parent.dnd_target_sound_el.length; se++) {
        //     sound_src = parent.dnd_target_sound_el[se];
        //     audio_el = $(sound_src).find(sound_element);
        //     audio_el.attr('src', $(sound_src).attr('data-audio'));
        // }

        // //play only one audio file at a time
        // document.addEventListener('play', function (e) {
        //     audios = document.getElementsByTagName('audio');
        //     for (var i = 0, len = audios.length; i < len; i++) {
        //         if (audios[i] != e.target) {
        //             audios[i].pause();
        //         }
        //     }
        // }, true);
    }
}

export default DranAndDrop