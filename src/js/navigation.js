"use strict"

import { hasClass } from './helpers'
import Page from './page'
import Map from './map'
import Slider from './slider'
import DranAndDrop from './dndOneToOne'
import Hotspot from './hotspot'
import Forms from './forms'
import Popup from './popup'
import MultipleChoice from './multipleChoice'
import IOLazy from './iolazy'

const Navigation = {
    init() {
        console.log('nav init')
        this.app_nav = document.querySelector('.app-nav')
        if (this.app_nav) {
            this.app_nav_button = this.app_nav.querySelector('button')
            this.app_nav_sidebar = this.app_nav.querySelector('.sidebar-nav')
            this.app_nav_sidebar_links = this.app_nav.querySelectorAll('.sidebar-nav li a') 
        }
        
        this.pages = document.querySelectorAll('.app-screen')
        this.map_controls = document.querySelector('.map-controls')
        this.popup_links = document.querySelectorAll('.map-popup .open-page')
        this.loader = document.querySelector('.loading')

        if (this.app_nav_button) {
            this.app_nav_button.addEventListener('pointerup', (e) => {
                TweenMax.killAll()
                this.openNav(e.target)
                e.stopPropagation()
            })
        }
        
        if (this.app_nav_sidebar_links) {
            Array.from(this.app_nav_sidebar_links).forEach((link, i) => {
                
                link.addEventListener('click', (e) => {
                    e.preventDefault()
                    this.loader.classList.remove('hidden')
                    TweenMax.killAll()
                    const reqURL = e.target.getAttribute('href')
                    if (reqURL === '') {
                        return
                    }
                    //request page
                    this.makeRequest(reqURL)
                    //push history state
                    this.appPushState(reqURL) 
                })
            })
        }

        if (this.popup_links) {
            Array.from(this.popup_links).forEach((link) => {
                const close_popup_btn = link.parentElement.querySelector('.close-popup-btn')
                close_popup_btn.addEventListener('pointerup', (e) => {
                    e.target.parentElement.classList.remove('visible')
                })

                link.addEventListener('click', (e) => {
                    e.preventDefault()

                    link.parentElement.classList.remove('visible')
        
                    this.loader.classList.remove('hidden')

                    TweenMax.killAll()

                    const reqURL = e.target.getAttribute('href')
                    if (reqURL === '') {
                        return
                    }
                    //request page
                    this.makeRequest(reqURL)
                    //push history state
                    this.appPushState(reqURL) 
                })
            })
        }  
    },
    progressBarNav() {
        const progress_bar_nav_links = document.querySelectorAll('.app-screen__nav li')
        if (progress_bar_nav_links) {
            //progress bar nav
            Array.from(progress_bar_nav_links).forEach((link, i) => link.addEventListener('pointerup', ((e) => {
                console.log('progressBarNav() ',e.target)
                const nav = link.closest('.app-screen__nav')
                const nav_id = nav.getAttribute('data-id')
                const slider = document.querySelector('.slider[data-id="' + nav_id + '"]')
                const slides = slider.querySelectorAll('.app-card')
                const slides_length = slides.length
                const next_button = document.querySelector('.page-nav[data-id="' + nav_id + '"].next')
                const prev_button = document.querySelector('.page-nav[data-id="' + nav_id + '"].prev')

                Array.from(nav.querySelectorAll('li')).forEach(link => {
                    //remove the active class from not current link
                    if (link !== e.target) {
                        link.classList.remove('active')
                    }
                })

                Array.from(slides).forEach((slide, slide_index) => {
                    //reset the link index to slide index
                    i = slide_index
                    //remove current from all slides
                    slide.classList.remove('current')
                    slide.querySelector('.inner-scroll').scrollTop = 0

                    //then add current to corresponding slide and a few more stuff
                    if (link.getAttribute('data-target') === slide.getAttribute('data-id')) {
                        //add class to current link
                        link.classList.add('visited')
                        link.classList.add('active')
                        //show corresponding slide
                        slide.classList.add('current')

                        IOLazy.addEventListeners(slide)
                        //reset slide index
                        Slider.setIndex(slide_index)
                        //disable nav buttons at start and end of cycle
                        if (next_button || prev_button) {
                            if (slide_index === slides_length - 1) {
                                next_button.classList.add('disabled')
                                prev_button.classList.remove('disabled')
                            } else if (slide_index === 0) {
                                prev_button.classList.add('disabled')
                                next_button.classList.remove('disabled')
                            } else {
                                next_button.classList.remove('disabled')
                                prev_button.classList.remove('disabled')
                            }
                        }
                    }
                })
            })))
        }
    },
    requestInfopage(url) {
        fetch(url)
            .then((response) => {
                // When the page is loaded convert it to text
                return response.text()
            })
            .then((data) => {
                console.log('loading info page')
                // Initialize the DOM parser
                const parser = new DOMParser()
                // Parse the text
                const doc = parser.parseFromString(data, "text/html")
                // Select part of the html
                const mainSection = doc.querySelector('.app-screen.page')

                const docArticle = mainSection.innerHTML
                //update page elements
                const page = document.querySelector('.app-screen.page')
                page.innerHTML = docArticle
                const images = page.querySelectorAll('img')
                Array.from(images).forEach(image => {
                    console.log(image, image.getAttribute('data-src'))

                   // image.classList.add('lazyload')
                    // if (image.getAttribute('srcset')) {
                    //     image.setAttribute('data-srcset', image.getAttribute('srcset'))
                    //     image.setAttribute('srcset', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7')
                    // }
                    //image.setAttribute('data-src', image.src)
                    image.setAttribute('src', image.getAttribute('data-src'))
                })
                
                const slug = url.substr(url.lastIndexOf('/') + 1)
                page.setAttribute('data-id', 'app-screen--' + slug)
                page.classList.add('app-screen--info')
                Page.init()
                Page.openPage(page)
                Slider.init()
                this.progressBarNav()
                
                this.loader.classList.add('hidden')

                const page_tabs = page.querySelectorAll('.app-screen__nav li')
                Array.from(page_tabs).forEach((tab, i) => {
                    if (i !== 0) {
                        tab.classList.remove('active')
                    } else {
                        tab.classList.add('active')
                    }
                })

                const mapContainer = document.getElementById('map-container')
                if (hasClass(mapContainer, 'overlay')) {
                    mapContainer.addEventListener('click', (e) => {
                        Page.closePage(page)
                    })
                }
            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            })
    },
    requestHomepage(url) {
        fetch(url)
            .then((response) => {
                // When the page is loaded convert it to text
                return response.text()
            })
            .then((data) => {
                // Initialize the DOM parser
                const parser = new DOMParser()
                // Parse the text
                const doc = parser.parseFromString(data, "text/html")
                // Select parts of the html
               
                const mainSection = doc.querySelector('#app')
                //const breadcrumbTrail = doc.querySelector('.breadcrumb-trail')
          
                const docArticle = mainSection.innerHTML
                //const docBreadcrumb = breadcrumbTrail.innerHTML
                const docTitle = doc.title
                //const docSkipLink = doc.querySelector('.skip-link').innerHTML

                //update page elements
               
                const home = document.querySelector('#app')
                home.innerHTML = docArticle
                // document.querySelector('.breadcrumb-trail').innerHTML = docBreadcrumb
                // document.querySelector('.skip-link').innerHTML = docSkipLink
                // // document.querySelector('.native-knowledge-logo a').innerHTML = docNKlogo
                // document.querySelector('html').classList.add('chapter')
                // document.querySelector('html').classList.remove('home')
                // document.title = docTitle
                // const slug = url.substr(url.lastIndexOf('/') + 1)
                // page.setAttribute('data-id', 'app-screen--' + slug)
                // Page.init()
                // Page.openPage(page)
                // DranAndDrop.init(page)
                // Hotspot.init(page)
                Navigation.init()
                Page.init()
            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            })
    },
    makeRequest(url) {
        fetch(url)
            .then((response) => {
                // When the page is loaded convert it to text
                return response.text()
            })
            .then((data) => {
                console.log('loading page')
                // Initialize the DOM parser
                const parser = new DOMParser()
                // Parse the text
                const doc = parser.parseFromString(data, "text/html")
                // Select part of the html
                const mainSection = doc.querySelector('.app-screen.page')
                //const breadcrumbTrail = doc.querySelector('.breadcrumb-trail')

                const docArticle = mainSection.innerHTML
                //const docBreadcrumb = breadcrumbTrail.innerHTML
                const docTitle = doc.title                
                //update page elements
                const page = document.querySelector('.app-screen.page')
                page.innerHTML = docArticle
                const images = page.querySelectorAll('img')
                Array.from(images).forEach(image => {
                    image.classList.add('lazyload')
                    if (image.getAttribute('srcset')) {
                        image.setAttribute('data-srcset', image.getAttribute('srcset'))
                        image.setAttribute('srcset', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7')
                    } else if (image.getAttribute('src')) {
                        image.setAttribute('data-src', image.getAttribute('src'))
                        image.setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7')
                    }
                })
                
                const slug = url.substr(url.lastIndexOf('/') + 1)
                this.loader.classList.add('hidden')
                page.setAttribute('data-id', 'app-screen--' + slug)
                page.classList.remove('app-screen--info')
                Page.init()
                Page.openPage(page)
                DranAndDrop.init(page)
                Hotspot.init(page)
                MultipleChoice.init()
                Forms.init()
                Slider.init()
                Popup.init('glossary-term')
                this.progressBarNav()

                const page_tabs = page.querySelectorAll('.app-screen__nav li')
                Array.from(page_tabs).forEach((tab, i) => {
                    if (i !== 0) {
                        tab.classList.remove('active')
                    } else {
                        tab.classList.add('active')
                    }
                })

                const customLinks = page.querySelectorAll('.custom-link')
                Array.from(customLinks).forEach((link, i) => {
                    link.addEventListener('click', (e) => {
                        e.preventDefault()
                        //kill any tweens still running
                        TweenMax.killAll()
                        //show loader
                        this.loader.classList.remove('hidden')
                        //close open page
                        Page.closePage(link.closest('.app-screen'))
                        //request new page
                        const reqURL = e.target.getAttribute('href')
                        if (reqURL === '') {
                            return
                        }
                        //request page
                        this.makeRequest(reqURL)
                        //push history state
                        this.appPushState(reqURL)
                    })
                })

                const goToHomepage = page.querySelector('.custom-link-home')
                if (goToHomepage) {
                    goToHomepage.addEventListener('click', (e) => {
                        e.preventDefault()
                        TweenMax.killAll()
                        //close open page
                        Page.closePage(goToHomepage.closest('.app-screen'))
                        Navigation.appPushState('/')
                    })
                }
                
                const mapContainer = document.getElementById('map-container')
                 if (hasClass(mapContainer, 'overlay')) {
                    mapContainer.addEventListener('click', (e) => {
                        Page.closePage(page)
                    })
                }
                if (document.documentElement.clientWidth > 1199) {
                    setTimeout(() => {
                        Map.resetMapZoom()
                    }, 1000)

                    const popups = document.querySelectorAll('.map-popup')

                    if (popups) {
                        Array.from(popups).forEach(popup => {
                            popup.classList.remove('visible')
                        })
                    } 
                }
                
                

            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            })
    },
    appPushState(url) {
        const pageInfo = {
            title: null,
            url: location.href
        }
        history.pushState(pageInfo, null, url)
    },
    openNav(el) {
        if (!el) el = document.getElementById('nav-icon')
        if (el) {
            if (hasClass(el, 'open')) {
                el.classList.remove('open')
            } else {
                el.classList.add('open')
            }
        } 
        
        this.app_nav_sidebar = document.querySelector('.sidebar-nav')
        this.app_nav_sidebar.classList.toggle('sidebar-nav--visible')
    }
}

export default Navigation