"use strict";

// import State from './state.js'
// import IdleTimeout from './idleTimeout.js'
// import MainScreen from './mainScreen.js'
import Forms from './forms'
import Slider from './slider'
import Map from './map'
import Navigation from './navigation'
import Page from './page'
import Popup from './popup'
import MultipleChoice from './multipleChoice'
import IOLazy from './iolazy'
import DranAndDrop from './dndOneToOne'
import Hotspot from './hotspot'
import { hasClass } from './helpers'

const WebApp = {
    init: function () {
        console.log('starting app!!')

        const images = document.querySelectorAll('img')
        const infoImages = document.querySelectorAll('.app-icons--info img')

        Array.from(images).forEach(image => {
            image.classList.add('lazyload')
            if (image.getAttribute('srcset')) {
                image.setAttribute('data-srcset', image.getAttribute('srcset'))
                image.setAttribute('srcset', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7')
            } else if (image.getAttribute('src')) {
                image.setAttribute('data-src', image.getAttribute('src'))
                image.setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7')
            }
        })

        Array.from(infoImages).forEach(image => {
            image.setAttribute('src', image.getAttribute('data-src'))
        })
        
        Slider.init()
        Navigation.init()
        Navigation.progressBarNav()
        MultipleChoice.init()
        Forms.init()
        Page.init()
        Map.init()

        window.USER_IS_TOUCHING = false

        window.addEventListener('touchstart', function onFirstTouch() {
            // we could use a class
            document.body.classList.add('user-is-touching')

            // or set some global variable
            window.USER_IS_TOUCHING = true

            // we only need to know once that a human touched the screen, so we can stop listening now
            window.removeEventListener('touchstart', onFirstTouch, false)
        }, false)

        Popup.init('glossary-term')
        // //handle application state
        // State.init()
        // //handle application idleTimeout
        // IdleTimeout.init()
        // //handle main app screen
        // MainScreen.init()
        // //handle app scrolling
        // window.addEventListener('wheel', function () { return false }, { passive: true });
        // document.getElementById('app').addEventListener('touchmove', function (e) {
        //     //only allow scolling via touch in selected areas like the slideup boxes
        //     // if (!$('.scroll-container .text').has($(e.target)).length) {
        //     //     e.preventDefault();
        //     // }
        //     e.preventDefault();
        // }, { passive: false });
        
    }
};

window.addEventListener('load', (e) => {
    WebApp.init()

    //execute the following functions if a case loads
    const page = document.querySelector('.app-screen.page')
    if (page.hasAttribute('data-id')) {
        const load_tl = new TimelineLite({
            onComplete: function () {
                document.getElementById('preloader').remove()

                Page.openPage(page)
                DranAndDrop.init(page)
                Hotspot.init(page)

                const pageOpen = document.querySelector('.page.open')
                if (pageOpen) {
                    const mapContainer = document.getElementById('map-container')
                    mapContainer.classList.add('overlay')
                    if (hasClass(mapContainer, 'overlay')) {
                        mapContainer.addEventListener('click', (e) => {
                            Page.closePage(pageOpen)
                        })
                    }
                }

                const page_tabs = page.querySelectorAll('.app-screen__nav li')
                const customLinks = page.querySelectorAll('.custom-link')
                const goToHomepage = page.querySelector('.custom-link-home')
                this.loader = document.querySelector('.loading')

                Array.from(page_tabs).forEach((tab, i) => {
                    if (i !== 0) {
                        tab.classList.remove('active')
                    } else {
                        tab.classList.add('active')
                    }
                })

                Array.from(customLinks).forEach((link, i) => {
                    link.addEventListener('click', (e) => {
                        e.preventDefault()
                        //kill any tweens still running
                        TweenMax.killAll()
                        //show loader
                        this.loader.classList.remove('hidden')
                        //close open page
                        Page.closePage(link.closest('.app-screen'))
                        //request new page
                        const reqURL = e.target.getAttribute('href')
                        if (reqURL === '') {
                            return
                        }
                        //request page
                        Navigation.makeRequest(reqURL)
                        //push history state
                        Navigation.appPushState(reqURL)
                    })
                })

                if (goToHomepage) {
                    goToHomepage.addEventListener('click', (e) => {
                        e.preventDefault()
                        TweenMax.killAll()
                        //close open page
                        Page.closePage(goToHomepage.closest('.app-screen'))
                        Navigation.appPushState('/')
                    })
                }
            }
        })
        load_tl.to(document.getElementById('preloader'), 0.3, {
            opacity: 0,
            zIndex: -1
        })
    } 
})













