"use strict"
import { hasClass, findHighestZIndex, removeClass, addClass, shuffle } from './helpers.js'
import { TweenMax, TimelineMax, TimelineLite } from '../../node_modules/gsap/TweenMax'
import { Draggable } from '../../node_modules/gsap/Draggable.js'

const Hotspot = {
    init(page) {
        this.hotspot_quiz = page.querySelectorAll('.quiz-hotspot')
        this.hotspot_draggables = []

        Array.from(this.hotspot_quiz).forEach((el, i) => {
            const quiz = el
            const quiz_id = quiz.getAttribute('id')

            const hotspot_reset = quiz.parentElement.querySelector('#' + quiz_id + '--reset')
            const hotspot_origin_container = quiz.querySelector('.hotspot-origin-container')
            const hotspot_target_container = quiz.querySelector('.hotspot-target-container')
            const snapX = []
            const snapY = []

            //items that will be dragged
            this.hotspot_origins = quiz.querySelectorAll('.hotspot-label')
            this.hotspot_origin_targets = []

            Array.from(this.hotspot_origins).forEach(el => {
                this.hotspot_origin_targets.push(el.querySelector('.drag-handle'))
            })

            this.hotspot_target_handle = hotspot_target_container.querySelectorAll('.drag-handle-target')

            Array.from(this.hotspot_origin_targets).filter((origin, i) => {
                if (i === 0) origin.classList.add('glow-infinite')
            })

            //add x,y coordinates as separate values to targets
            Array.from(this.hotspot_target_handle).forEach(el => {
                const hotspot_coordinates = el.getAttribute('data-coordinates')
                //get image coordinates separately to assign as custom attributes (data_x) and (data_y)
                const hotspot_coordinates_x = hotspot_coordinates.substring(0, hotspot_coordinates.indexOf(','))
                const hotspot_coordinates_y = hotspot_coordinates.split(',')[1]
                //image coordinates are in percentages, convert them to pixels to 
                //assign as custom attributes to the draggable element
                const newX = Math.round(parseFloat(hotspot_coordinates_x) - 3)
                const newY = Math.round(parseFloat(hotspot_coordinates_y) - 4)
                const pixels_x = hotspot_target_container.clientWidth * (newX / 100)
                const pixels_y = hotspot_target_container.clientHeight * (newY / 100)

                el.setAttribute('data_x', Math.round(parseFloat(pixels_x)));
                el.setAttribute('data_y', Math.round(parseFloat(pixels_y)));
                el.setAttribute('style', "left: " + Math.round(parseFloat(hotspot_coordinates_x) - 3) + "%;" + "top: " + Math.round(parseFloat(hotspot_coordinates_y) - 4) + "%;");

                //add drag snap points to arrays - for draggable elements
                snapX.push(pixels_x);
                snapY.push(pixels_y);
            })
            
            if (hasClass(el, 'drag-and-drop')) {
                this.dragHotspotElements(el, i)
                this.checkHotspotAnswers(quiz)
                this.showHotspotCorrectAnswers(quiz)
                hotspot_reset.addEventListener('pointerdown', (e) => {
                    this.resetHotspot(quiz)
                })
            }

            if (hasClass(el, 'mouseover')) {
                //parent.term_popup('.hotspot', 'data-title');
            }
           
            
        })
    },
    dragHotspotElements(quiz, index) {
        index = index + 1 //drag and drop counter starts at 1
        const quiz_id = quiz.getAttribute('data-id')
        //make all the targets available on quiz load
        Array.from(document.querySelectorAll('#' + quiz_id + ' .drag-handle-target')).forEach(el => {
            el.classList.add('available')
        })

        const draggables = Draggable.create('#' + quiz_id + ' .drag-handle', {
            type: "x,y",
            bounds: ('[data-id="' + quiz_id + '"]'),
            allowNativeTouchScrolling: false,
            onPressInit: function () {
                //set variables to use later
                this.hotspot_overlapThreshold = "40%";
                this.hotspot_show_correct = document.querySelector('#' + quiz_id + '--show-correct-answers')
                this.hotspot_check = document.querySelector('#' + quiz_id + '--check-answers')
                this.hotspot_reset = document.querySelector('#' + quiz_id + '--reset')
                this.hotspot_prompt = document.querySelector('#' + quiz_id + '--prompt')
                this.hotspot_origin_container = this.target.closest('.quiz-hotspot').querySelector('.hotspot-label-list')
                this.hotspot_target_container = this.target.closest('.quiz-hotspot').querySelector('.hotspot-target-container')
                // record the starting values so we can compare them later...
                // for each target find its position in the bounds container and assign it to the 
                // origin, doing this here because hidden elements do not have offsets, needs to be
                // retriggered
                this.hotspot_targets = this.hotspot_target_container.querySelectorAll('.drag-handle-target')
                this.hotspot_available_targets = this.hotspot_target_container.querySelectorAll('.available')
                this.positioned = this.hotspot_origin_container.querySelectorAll('.positioned')

                //highlight the origin text while dragging
                Array.from(this.hotspot_origin_container.querySelectorAll('.drag-handle')).forEach(el => {
                    const origin = this.hotspot_origin_container.querySelector('[data-identifier="' + this.target.getAttribute('data-identifier') + '"]')
                    origin.parentElement.classList.add('highlighted')
                    el.classList.remove('glow-infinite')
                })

                this.target.classList.add('static-glow')

            },
            onDragStart: function () {
                //show draggable clones
                Array.from(this.hotspot_origin_container.querySelectorAll('.clone')).forEach(el => {
                    el.classList.remove('visually-hidden')
                })
                // //if drag starts while in drop area just return point to position
                Array.from(this.hotspot_targets).forEach(el => {
                    if (el.getAttribute('data-hit') !== '') {
                        if (this.hitTest(el, this.hotspot_overlapThreshold)) {
                            el.classList.add('available')
                            el.classList.remove('correct')
                            el.classList.remove('wrong')
                        }
                    }
                })
                // //hide reset button
                this.hotspot_show_correct.classList.add('visually-hidden')
                this.hotspot_check.classList.remove('visually-hidden')
                //clear the prompt message box
                this.hotspot_prompt.innerHTML = ''
                //remove correct and wrong highlight classes from targets
                Array.from(this.positioned).forEach(el => {
                    el.classList.remove('highlight-correct')
                    el.classList.remove('highlight-wrong')
                })
            },
            onDrag: function () {
                Array.from(this.hotspot_targets).forEach(target => {
                    
                    if (this.hitTest(target, this.hotspot_overlapThreshold)) {
                        //if draggable in target range highlight it and animate it
                        this.target.classList.remove('static-glow')
                        target.classList.add('showOver')
                    } else {
                        //this.target.classList.add('static-glow')
                        target.classList.remove('showOver')

                        if (target.getAttribute('data-hit') === this.target.getAttribute('data-identifier')) {
                            //on drag start check the data-hit attribute - if it has a value it is because a draggable instance has been assigned to it and passed it its data-identifier attribute
                            //if it has a value it means draggable was there
                            //remove the data-hit attribute value and signify that it is available again
                            target.removeAttribute('data-hit')
                            target.classList.add('available')
                        }
                    }
                })
            },
            onDragEnd: function (e) {
                let snapMade = false

                Array.from(this.hotspot_targets).forEach(el => {
                    
                    const current_hotspot_target = el
                    const identifier = current_hotspot_target.getAttribute('data-identifier')
                    
                    if (this.hitTest(current_hotspot_target, this.hotspot_overlapThreshold)) {
                        snapMade = true
                        //connect source and target via an identifier
                        //give the draggable item an attr of data-target with a value of the hit id
                        //also give it a couple of styling classes
                        this.target.setAttribute('data-target', identifier)
                        this.target.classList.add("positioned")
                        //after a hit has been made reveal the check hotspot button
                        this.hotspot_check.classList.remove('disabled')
                        this.hotspot_check.classList.remove('visually-hidden')
                        //if the item has been positioned correctly, assign styling classes
                        if (this.target.getAttribute('data-target') === this.target.getAttribute('data-identifier')) {
                            this.target.classList.remove("wrong")
                            this.target.classList.remove("highlight-wrong")
                            this.target.classList.remove("correct")
                            this.target.classList.remove("highlight-correct")
                            this.target.classList.add("correct")
                        } else if (this.target.getAttribute('data-target') !== this.target.getAttribute('data-identifier')) {
                            this.target.classList.remove("wrong")
                            this.target.classList.remove("highlight-wrong")
                            this.target.classList.remove("correct")
                            this.target.classList.remove("highlight-correct")
                            this.target.classList.add("wrong")
                        }

                        if (hasClass(current_hotspot_target, 'available')) {
                            //if there isn't one there already
                            //give the target an attribute of data-hit to mark that there has been a match
                            current_hotspot_target.setAttribute('data-hit', this.target.getAttribute('data-identifier'))
                            //move item to position if position available and overlapThreshold condition is met
                            const tl = new TimelineLite()

                            tl
                                .to(this.target, 0.1, {
                                    top: current_hotspot_target.getAttribute('data_y'),
                                    left: current_hotspot_target.getAttribute('data_x')
                                })
                                .to(this.target, 0.1, {
                                    x: this.minX + parseInt(current_hotspot_target.getAttribute('data_x')) - 6,
                                    y: parseInt(current_hotspot_target.getAttribute('data_y')) + this.minY - 6,
                                    rotation: 0.01//firefox efing bug
                                });
                        } else {
                            console.log(this.hotspot_prompt)
                            //if the position isn't available (does not have an available class) send it back to its starting position)
                            this.hotspot_prompt.classList.remove('correct')
                            this.hotspot_prompt.classList.add('wrong')
                            this.hotspot_prompt.innerHTML = 'That position is not available, try a different one!'
                            this.target.classList.remove("positioned")
                            this.target.classList.remove("wrong")
                            this.target.classList.remove("highlight-wrong")
                            this.target.classList.remove("correct")
                            this.target.classList.remove("highlight-correct")

                            //show a message that an item has already been placed there and return item to starting position
                            const tl_return = new TimelineLite()
                            
                            tl_return
                                .to(this.target, 0.1, {
                                    opacity: 1,
                                    x: 0,
                                    y: 0,
                                    rotation: 0.01//firefox efing bug
                                })
                        }
                        //if there is a current target (i.e. a match has been made, don't allow it to be a target as long as its populated with another draggable item)
                        current_hotspot_target.classList.remove('available')
                    }
                })

                if (!snapMade) {
                    this.target.classList.remove("positioned")
                    this.target.classList.remove("wrong")
                    this.target.classList.remove("highlight-wrong")
                    this.target.classList.remove("correct")
                    this.target.classList.remove("highlight-correct")
                    this.target.classList.remove("showOver")
                    this.target.classList.remove("ripple")

                    this.positioned = this.hotspot_origin_container.querySelectorAll('.positioned')

                    if (this.positioned.length === 0) {
                        //only show the check hotspot button if at least an item has been dragged
                        this.hotspot_check.classList.add('disabled')
                    } else if (this.positioned.length !== 0) {
                        this.hotspot_check.classList.remove('disabled')
                    }

                    this.hotspot_reset.classList.add('visually-hidden')
                    this.target.setAttribute('data-target', '')
                    TweenLite.to(this.target, 0.2, {
                        css: {
                            x: 0,
                            y: 0,
                            rotation: 0.01//firefox efing bug
                        }
                    })

                    //find the target with the same data-target attribute as this.target
                    const drag_handle_targets = document.querySelectorAll('.drag-handle-target')
                    Array.from(drag_handle_targets).forEach(el => {
                        el.classList.remove('showOver')
                    })
                }
                //when hovering over a positioned element highlight the origin text
                Array.from(document.querySelectorAll('.drag-handle.positioned')).forEach(el => {
                    const origin = this.hotspot_origin_container.querySelector('[data-identifier="' + el.getAttribute('data-identifier') + '"]')
                    el.addEventListener('mouseover', (e) => {
                        origin.parentElement.classList.add('highlighted')
                    })

                    el.addEventListener('mouseout', (e) => {
                        origin.parentElement.classList.remove('highlighted')
                    })
                })

            },
            onRelease: function (e) {
                //unhighlight the origin text after 1 1/2 a second
                Array.from(document.querySelectorAll('.drag-handle')).forEach(el => {
                    const origin = this.hotspot_origin_container.querySelector('[data-identifier="' + this.target.getAttribute('data-identifier') + '"]')
                    origin.parentElement.classList.remove('highlighted')
                })

                this.target.classList.remove('static-glow')
            }
        })

        draggables.index = index
    },
    checkHotspotAnswers(quiz) {
        const quiz_id = quiz.getAttribute('id')
        const hotspot_check = document.querySelector('#' + quiz_id + '--check-answers')
        const hotspot_show_correct = document.querySelector('#' + quiz_id + '--show-correct-answers')
        const hotspot_prompt = document.querySelector('#' + quiz_id + '--prompt')
        const hotspot_reset = document.querySelector('#' + quiz_id + '--reset')

        hotspot_check.addEventListener('pointerup', (e) => {
            const hotspot_check_id = e.target.getAttribute('id')
            const hotspot_id = hotspot_check_id.substring(0, hotspot_check_id.indexOf('--'))
            const draggable_item = document.querySelectorAll('#' + hotspot_id + ' .drag-handle')
            const draggable_target = document.querySelectorAll('#' + hotspot_id + ' .drag-handle-target')
            const correct = document.querySelectorAll('#' + hotspot_id + ' .drag-handle.correct')
            const wrong = document.querySelectorAll('#' + hotspot_id + ' .drag-handle.wrong')

            Array.from(correct).forEach(el => {
                el.classList.add('highlight-correct')
                let parent = document.querySelector('.hotspot-target[data-identifier="' + el.getAttribute('data-target') + '"]')
                if (parent) {
                    parent.classList.add('correct')
                }
            })

            Array.from(wrong).forEach(el => {
                el.classList.add('highlight-wrong')
                let parent = document.querySelector('.hotspot-target[data-identifier="' + el.getAttribute('data-target') + '"]')
                if (parent) {
                    parent.classList.add('wrong')
                }
            })

            Array.from(draggable_target).forEach(el => {
                el.classList.remove('showOver')
                el.classList.remove('ripple')
            })

            const positioned = document.querySelectorAll('#' + hotspot_id + ' .positioned')
            const correct_positioned = document.querySelectorAll('#' + hotspot_id + ' .positioned.correct')

            if (draggable_item.length === correct_positioned.length) {
                hotspot_reset.classList.remove('visually-hidden')
                hotspot_check.classList.add('visually-hidden')

                hotspot_prompt.classList.remove('wrong')
                hotspot_prompt.classList.add('correct')
                hotspot_prompt.innerHTML = 'Well done! All answers correct!'

                Array.from(draggable_item).forEach(el => {
                    el.classList.add('disabled')
                })

            } else if (draggable_item.length === positioned.length && positioned.length !== correct_positioned.length) {
                setTimeout(() => {
                    hotspot_check.classList.add('visually-hidden')
                    hotspot_show_correct.classList.remove('visually-hidden')
                }, 100)
            }
        })
    },
    showHotspotCorrectAnswers(quiz) {
        const quiz_id = quiz.getAttribute('id')
        const hotspot_show_correct = document.querySelector('#' + quiz_id + '--show-correct-answers')
        const hotspot_reset = document.querySelector('#' + quiz_id + '--reset')

        hotspot_show_correct.addEventListener('pointerdown', (e) => {
            const hotspot_check_id = e.target.getAttribute('id')
            const hotspot_id = hotspot_check_id.substring(0, hotspot_check_id.indexOf('--'))
            const right_positions = document.querySelectorAll('#' + hotspot_id + ' .right-positions')
            const draggable_item = document.querySelectorAll('#' + hotspot_id + ' .drag-handle')
            const clones = document.querySelectorAll('#' + hotspot_id + ' .clone')
            
            var ts = new TimelineMax()
            ts
                .set(draggable_item, {
                    x: 0,
                    y: 0,
                    zIndex: -1,
                    rotation: 0.01//firefox efing bug 
                })

            Array.from(clones).forEach(el => {
                el.classList.remove('default')
            })

            Array.from(draggable_item).forEach(el => {
                el.classList.remove('positioned')
                el.classList.remove('highlight-correct')
                el.classList.remove('highlight-wrong')
                el.classList.remove('wrong')
                el.classList.remove('correct')
                el.classList.add('disabled')
            })

            var hotspot_correct_answers = []
            var hotspot_correct_answer_parent = []

            Array.from(right_positions).forEach(el => {
                el.classList.remove('no-opacity')
                hotspot_correct_answers.push(el)
                var el_parent = el.parentElement.closest('.hotspot-target')
                hotspot_correct_answer_parent.push(el_parent)
            })

            var tl = new TimelineMax()
            tl.staggerFromTo(hotspot_correct_answers, 0.6,
                {
                    css: {
                        'opacity': 0,
                    },
                },
                {
                    css: {
                        'opacity': 1
                    }
                },
                0.3)

            hotspot_reset.classList.remove('visually-hidden')
            hotspot_show_correct.classList.add('visually-hidden')
            hotspot_show_correct.classList.remove('disabled')
        })
    },
    resetHotspot(quiz) {
        if (quiz) {
            const quiz_id = quiz.getAttribute('id')
            const right_positions = quiz.querySelectorAll('.right-positions')
            const draggable_items = quiz.querySelectorAll('.drag-handle')
            const draggable_targets = quiz.querySelectorAll('.drag-handle-target')
            const target_container = quiz.querySelector('.hotspot-target-container')
            const hotspot_show_correct = document.querySelector('#' + quiz_id + '--show-correct-answers')
            const hotspot_check = document.querySelector('#' + quiz_id + '--check-answers')
            const hotspot_reset = document.querySelector('#' + quiz_id + '--reset')
            const prompt = document.querySelector('#' + quiz_id + '--prompt')

            prompt.classList.add('default')
            prompt.classList.remove('correct')
            prompt.innerHTML = ''

            Array.from(right_positions).forEach(el => {
                el.removeAttribute('style')
                el.classList.add('no-opacity')
            })

            Array.from(draggable_items).forEach((el, i) => {
                el.classList.remove('positioned')
                el.classList.remove('correct')
                el.classList.remove('wrong')
                el.classList.remove('highlight-wrong')
                el.classList.remove('highlight-correct')
                el.classList.remove('ripple')
                el.classList.remove('disabled')
                if (i === 0) {
                    el.classList.add('glow-infinite')
                }
            })

            Array.from(draggable_targets).forEach(el => {
                console.log(el)
                el.classList.remove('showOver')
                el.classList.remove('ripple')
                el.classList.remove('correct')
                el.classList.remove('wrong')
                el.classList.add('available')
                el.setAttribute('data-hit', '')
            })

            const tl = new TimelineLite()
            tl
                .set(draggable_items, {
                    zIndex: 1,
                    opacity: 1,
                    x: 0,
                    y: 0,
                    rotation: 0.01//firefox efing bug
                })

            setTimeout(() => {
                hotspot_show_correct.classList.add('visually-hidden')
                hotspot_check.classList.add('disabled')
                hotspot_check.classList.remove('visually-hidden')
                hotspot_reset.classList.add('visually-hidden')
            }, 100)

            //kill everything
            TweenMax.killAll()
        }
    }
}

export default Hotspot