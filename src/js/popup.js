import { hasClass } from "./helpers";

"use strict";

const Popup = {
    init(term_class, text_attribute) {
        const term = document.querySelectorAll('.' + term_class)
        //for each glossary term in the content 
        // 1. create a popup
        // 2. add a click event
        Array.from(term).forEach((el, i) => {
            el.setAttribute('data-id', term_class + '-' + i)
            this.createTermPopup(el) /*1*/

            el.addEventListener('mouseover', this.openTermPopup, { passive: true }) /*2*/

            if (USER_IS_TOUCHING) {
                el.addEventListener('touchstart', this.openTermPopup, { passive: true }) /*2*/
            }
        })

        if (!USER_IS_TOUCHING) {
            document.addEventListener('pointerup', (e) => {
                document.querySelectorAll('.term-popup.open').forEach(el => {
                    el.classList.remove('open')
                })
            })

            document.querySelectorAll('.term-popup').forEach(el => {
                el.addEventListener('pointerup', (e) => {
                    if (e.target !== el.querySelector('.close-popup')) {
                        e.stopPropagation()
                    }
                }, true)
            })
        }
    },
    createTermPopup(element) { 
        if (element.getAttribute('data-title') === null) { return }

        //doing this mainly for IE11 
        if (element.getAttribute('data-title')) {
            this.term_title = element.getAttribute('data-title')
        }

        if (element.getAttribute('data-description') || element.getAttribute('data-html')) {
            this.term_description = element.getAttribute('data-description') || element.getAttribute('data-html');
        }
        
        this.term_popup_container = document.createElement('div')
        this.term_popup_container.classList.add('term-popup-container')
        this.term_popup = document.createElement('aside') 
        this.term_popup.classList.add('term-popup')
        this.term_popup.setAttribute('data-id', element.getAttribute('data-id'))
        this.term_popup.innerHTML = '<h4>' + this.term_title + '</h4><p>' + this.term_description + '</p>'
        //this.term_popup.style.top = element.getBoundingClientRect().top - 120 + 'px'
        this.term_popup_container.appendChild(this.term_popup)
        if (element) {
            element.insertAdjacentElement('afterend', this.term_popup_container);
        }

        this.close_popup = document.createElement('i') 
        this.close_popup.classList.add('icon')
        this.close_popup.classList.add('close-popup')
        this.close_popup.classList.add('term-close')

        this.term_popup.appendChild(this.close_popup)

        this.close_popup.addEventListener('pointerup', (e) => {
            event.preventDefault()
            e.target.parentElement.classList.remove('open')
        })
    },
    openTermPopup(element) {
        if (element.target !== this) {
            return
        }

        //get the target's popup
        const term_popup = document.querySelector('.term-popup[data-id="' + element.target.getAttribute("data-id") + '"]')
        const term_popup_open = document.querySelectorAll('.term-popup.open')

        if (!hasClass(term_popup, 'open')) {
            term_popup.style.left = ''
            term_popup.style.top = ''

            Array.from(term_popup_open).forEach(el => {
                if (el !== element.target) {
                    el.classList.remove('open')
                }
            })
            //set vars
            let trigger_position = element.target.getBoundingClientRect()
            let trigger_width = element.target.getBoundingClientRect().width
            let term_popup_width = 460
            let page_width = element.target.closest('.inner-scroll').clientWidth
            let page_height = element.target.closest('.inner-scroll').clientHeight
            let pageOffsetX = element.target.closest('.inner-scroll').getBoundingClientRect().left
            
            //position the popup relative to the trigger on the left & right
            //if the popup is relative to a trigger positioned absolutely (hotspot image)
            if (hasClass(element.target.parentElement, 'hotspot-image')) {
                term_popup.parentElement.style.position = 'absolute'
                term_popup.parentElement.style.left = element.target.style.left
                term_popup.parentElement.style.top = element.target.style.top
                term_popup.style.left = '50%'
                term_popup.style.marginLeft = -(term_popup_width / 2) + 'px'
            } else {
                //if the popup is inline in the text
                term_popup.style.left = -element.target.offsetLeft + 'px'
                //console.log(term_popup_width, page_width, pageOffsetX, parseInt(page_width - trigger_position.left)))

                // if (term_popup_width > parseInt(page_width - trigger_position.left)) {
                //     term_popup.style.right = '0'
                // } else if (parseInt(trigger_position.left) < term_popup_width / 2) {
                //     term_popup.style.left = '0'
                // } else {
                //     term_popup.style.left = (-term_popup_width / 2) - (parseInt(element.target.clientWidth) / 2) + 'px'
                // }
                term_popup.style.left = -(term_popup_width / 2 + trigger_width / 2)   + 'px'
            }
            //hide/show the popup by toggling the open class
            term_popup.classList.add('open')
            //get the term popup height after display 
            let term_popup_height = term_popup.clientHeight
            //calculate top position after having opened the popup to calculate the height
            if (trigger_position.top + parseInt(term_popup_height) >= page_height) {
                term_popup.style.top = -term_popup_height - 35 + 'px'
            }
            
        }
    }
}

export default Popup