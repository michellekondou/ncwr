
// data
const adminURL = process.env.NODE_ENV === 'production' ? process.env.STAGING_ADMIN_URL : process.env.LOCAL_ADMIN_URL; 
const baseURL = `${adminURL}/wp-json/wp/v2`
const ACFendoint = `${adminURL}/wp-json/acf/v3`
const postTitlesURL = `${adminURL}/wp-json/myplugin/v1/post-titles`
const toJSON = res => res.json();

const posts = () => {
    return fetch(`${baseURL}/posts/?per_page=100`)
        .then(toJSON)
        .then(data => {
            return data
        })
}

const postBySlug = (_, args) => {
    const { slug } = args
    return fetch(`${baseURL}/posts/?slug=${slug}`)
        .then(toJSON)
        .then(data => {
            return data
        })
}

const postTitle = () => {
    return fetch(`${postTitlesURL}`)
        .then(toJSON)
        .then(data => {
            return data
        })
}

export { posts, postTitle, postBySlug }
//next step resolvers
