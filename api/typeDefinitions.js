const typeDefs = `
  type Query {
    posts: [Post]
    postTitle: [PostTitle]
    postBySlug(slug: String): [Post]
  }

  type PostTitle @cacheControl(maxAge: 240) {
    ID: ID
    post_name: String
    post_title: String
    slug: String
    acf: Acf
  }

  type Post @cacheControl(maxAge: 240) {
    id: ID
    slug: String
    acf: Acf
  }

  type Acf {
    card_title: String
    card_summary: String
    fact_sheet: factSheetOptions
    card_image: String
    cards: [Card]
    tabs: [Tab]
    intro_text: String
  }

  type factSheetOptions {
    url: String
    title: String
    filename: String
    filesize: String
    mime_type: String
  }

  type Tab {
    heading: String
    block_1: String
    block_2: String
  }

  interface Card {
    acf_fc_layout: String
  }

  type Text implements Card {
    acf_fc_layout: String
    type: String
    text: String
  }

  type Intro implements Card {
    acf_fc_layout: String
    block_1: String
    block_2: String
    block_3: String
    level: String
    duration: String
    classroom: Boolean
  }

  type MultipleChoice implements Card {
    acf_fc_layout: String
    quiz_form_url: String
    quiz_title: String
    quiz_extended_text: String
    quiz_field_name: String
    quiz_thank_you_msg: String
    quiz_options: [InputOptions]
  }

  type Dragndrop implements Card {
    acf_fc_layout: String
    intro_text: String
    items: [DragndropOptions]
    origin_heading: String
    target_heading: String
  }

  type DragndropOptions {
    option: String
    target: String
    target_sound_file: String  
  }

  type MultipleChoiceMultiple implements Card {
    acf_fc_layout: String
    quiz_description: String
    quiz: [Quiz]
  }

  type MultipleChoiceGrid implements Card {
    acf_fc_layout: String
    quiz_description: String
    quiz: [QuizGrid]
  }

  type QuizGrid {
    quiz_input_value: String
    quiz_options: [QuizGridOptions]
  }

  type QuizGridOptions {
    quiz_title: String
    quiz_extended_text: String
    quiz_field_name: String
    quiz_tip: String
  }

  type Likert implements Card {
    acf_fc_layout: String
    quiz_title: String
    quiz_field_name: String
    quiz_thank_you_msg: String
    quiz_options: [LikertOptions]
  }

  type LikertOptions {
    quiz_input_value: String
  }

  type LikertGrid implements Card {
    acf_fc_layout: String
    quiz_title: String
    quiz_thank_you_msg: String
    quiz_options: [LikertGridOptions]
  }

  type LikertGridOptions {
    quiz_question: String
    quiz_field_name: String
  }

  type Hotspot implements Card {
    acf_fc_layout: String
    hotspot_description: String
    hotspot_type: String
    hotspots: [HotspotData]
    hotspot_file: ImgOptions
  }

  type HotspotData {
    hotspot_coordinates: String
    hotspot_title: String
    hotspot_description: String
  }

  type Checkbox implements Card {
    acf_fc_layout: String
    quiz_title: String
    quiz_field_name: String
    quiz_thank_you_msg: String
    quiz_options: [InputOptions]
  }

  type ShortText implements Card {
    acf_fc_layout: String
    form_title: String
    form_field_name: String
  }

  type FeedBack implements Card {
    acf_fc_layout: String
    feedback_type: [FeedbackType]
  }

  interface FeedbackType {
    acf_fc_layout: String
  }

  type FeedbackShorText implements FeedbackType {
    acf_fc_layout: String
    form_url: String
    form_description: String
    form_field_name: String
  }

  type FeedbackLikert implements FeedbackType {
    acf_fc_layout: String
    quiz_title: String
    quiz_field_name: String
    quiz_thank_you_msg: String
    quiz_options: [LikertOptions]
  }

  type FeedbackMultipleChoice implements FeedbackType {
    acf_fc_layout: String
    quiz_form_url: String
    quiz_title: String
    quiz_extended_text: String
    quiz_field_name: String
    quiz_thank_you_msg: String
    quiz_options: [InputOptions]
  }

  type FeedbackText implements FeedbackType {
    acf_fc_layout: String
    text: String
  }

  type Quiz {
    quiz_form_url: String
    quiz_title: String
    quiz_extended_text: String
    quiz_field_name: String
    quiz_thank_you_message: String
    quiz_options: [QuizOptions]
  }

  type QuizOptions {
    quiz_input_value: String
    quiz_input_type: String
  }

  type InputOptions {
    quiz_input_value: String
    photo: ImgOptions
    quiz_prompt: String
    quiz_input_type: String
  }

  type ImgOptions {
    ID: ID
    url: String
    title: String
    width: String
    height: String
  }

`;

export default typeDefs