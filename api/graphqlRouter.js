const { createApolloFetch } = require('apollo-fetch')

const graphqlURL = process.env.NODE_ENV === 'production' ? process.env.STAGING_GRAPHQL_URL : process.env.LOCAL_GRAPHQL_URL;
const apolloFetch = createApolloFetch({ uri: graphqlURL }) //replace the url to reflect the online address when deploying -  don't hardcode, use variables

//get data for templates
const getPosts = function () {
  //the following query must live in resolvers.js
  //this is the query I would type in graphiql
  const query = `
    query posts
    {
      posts {
        id
        slug
        acf {
          card_title
          card_summary
        }
      }
    }
  `
  const result = apolloFetch({ query }).then(res => {
    return res.data
  })

  return result
}

const getInfo = function () {
  //the following query must live in resolvers.js
  //this is the query I would type in graphiql
  const query = `
    query posts
    {
      posts {
        id
        acf {
           tabs {
            heading
            block_1
            block_2
          }  
        }
      }   
    }
  `
  const result = apolloFetch({ query }).then(res => {
    return res.data
  })

  return result
}

// const getPostById = function (postId) {
//   //the following query must live in user.resolvers.js line 64
//   //this is the query I would type in graphiql
//   const query = `
//     query postById($postId: ID)
//       {
//         postId(id: $postId) {
//           id
//           acf {
//             tabs {
//               heading
//               block_1
//               block_2
//             }  
//           }
//         }
//       }          
//   `
//   const variables = {
//     postId: postId
//   }

//   const result = apolloFetch({ query, variables }).then(res => {
//     console.log('graphqlrouter: result:', res.data)
//     return res.data
//   })
  
//   return result
// }

const getPostTitles = function () {
  //the following query must live in resolvers.js
  //this is the query I would type in graphiql
  const query = `
    query postTitle
    {
      postTitle {
        ID
        post_name
        post_title
      }
    }
  `
  const result = apolloFetch({ query }).then(res => {
    return res.data
  })

  return result
}

const getPostBySlug = function (postSlug) {
  //the following query must live in user.resolvers.js line 64
  //this is the query I would type in graphiql
  const query = `
    query postBySlug($postSlug: String)
      {
        postBySlug(slug: $postSlug) {
        slug
        acf {
            card_title
            card_summary
            fact_sheet {
              url
              title
              filename
              mime_type
            }
            tabs {
              heading
              block_1
              block_2
            }  
            cards {
              acf_fc_layout
              ... on Intro {
                block_1
                block_2
                block_3
                level
                duration
                classroom
              }
              ... on Text {
                type
                text
              }
              ... on Likert {
                quiz_title
                quiz_field_name
                quiz_thank_you_msg
                quiz_options {
                  quiz_input_value
                }
              }
              ... on LikertGrid {
                quiz_title
                quiz_thank_you_msg
                quiz_options {
                  quiz_question
                  quiz_field_name
                }
              }
              ... on ShortText {
                form_title
                form_field_name
              }
              ... on MultipleChoice {
                quiz_title
                quiz_extended_text
                quiz_field_name
                quiz_thank_you_msg
                quiz_options {
                  quiz_input_value
                  photo {
                    ID
                    url
                  }
                  quiz_prompt
                  quiz_input_type
                }
              }
              ... on MultipleChoiceMultiple {
                quiz_description
                quiz {
                  quiz_form_url
                  quiz_title
                  quiz_extended_text
                  quiz_field_name
                  quiz_thank_you_message
                  quiz_options {
                    quiz_input_value
                    quiz_input_type
                  }
                }
              }
              ... on MultipleChoiceGrid {
                quiz_description
                quiz {
                  quiz_input_value
                  quiz_options {
                    quiz_title
                    quiz_extended_text
                    quiz_field_name
                    quiz_tip
                  }
                }
              }
              ... on FeedBack {
                acf_fc_layout
                feedback_type {
                  acf_fc_layout
                  ... on FeedbackShorText {
                    form_url
                    form_field_name
                    form_description
                  }
                  ... on FeedbackLikert {
                    quiz_title
                    quiz_options {
                      quiz_input_value
                    }
                    quiz_field_name
                    quiz_thank_you_msg
                  }
                  ... on FeedbackMultipleChoice {
                    quiz_form_url
                    quiz_title
                    quiz_field_name
                    quiz_extended_text
                    quiz_thank_you_msg
                    quiz_options {
                      quiz_prompt
                      quiz_input_type
                      quiz_input_value
                    }
                  }
                  ... on FeedbackText {
                    text
                  }
                }
              }
              ... on Hotspot {
                hotspot_description
                hotspot_type
                hotspot_file {
                  ID
                  url
                  width
                  height
                }
                hotspots {
                  hotspot_coordinates
                  hotspot_title
                  hotspot_description
                }
              }
              ... on Dragndrop {
                intro_text
                items {
                  option
                  target
                  target_sound_file
                }
                origin_heading
                target_heading
              }
              ... on ShortText {
                form_title
                form_field_name
              }
              ... on Checkbox {
                quiz_title
                quiz_field_name
                quiz_thank_you_msg
                quiz_options {
                  quiz_input_value
                  photo {
                    ID
                    url
                  }
                quiz_prompt
                quiz_input_type
              }
            }
          }
        }
      }
    }
  `
  const variables = {
    postSlug: postSlug
  }

  const result = apolloFetch({ query, variables }).then(res => {
    return res.data
  })

  return result
}

export { getPosts, getPostById, getPostTitles, getPostBySlug }