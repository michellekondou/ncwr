import { posts, postById, postTitle, postBySlug } from './controllers'

const resolvers = {
    Query: {
        posts,
        postTitle,
        postBySlug
    },
    //the following is necessary to implement interfaces in apollo graphql
    Card: {
        __resolveType(obj, context, info) {
            if (obj.acf_fc_layout === "intro") {
                return 'Intro';
            }

            if (obj.acf_fc_layout === "multiple_choice") {
                return 'MultipleChoice';
            }

            if (obj.acf_fc_layout === "text") {
                return 'Text';
            }

            if (obj.acf_fc_layout === "dragndrop") {
                return 'Dragndrop';
            }

            if (obj.acf_fc_layout === "feedback") {
                return 'FeedBack';
            }

            if (obj.acf_fc_layout === "likert") {
                return 'Likert';
            }

            if (obj.acf_fc_layout === "likert_grid") {
                return 'LikertGrid';
            }

            if (obj.acf_fc_layout === "hotspot") {
                return 'Hotspot';
            }

            if (obj.acf_fc_layout === "short_text") {
                return 'ShortText';
            }

            if (obj.acf_fc_layout === "checkbox") {
                return 'Checkbox';
            }

            if (obj.acf_fc_layout === "multiple_choice_multiple") {
                return 'MultipleChoiceMultiple';
            }

            if (obj.acf_fc_layout === "multiple_choice_grid") {
                return 'MultipleChoiceGrid';
            }

            return null;
        }
    },
    FeedbackType: {
        __resolveType(obj, context, info) {

            if (obj.acf_fc_layout === "feedback_short_text") {
                return 'FeedbackShorText';
            }

            if (obj.acf_fc_layout === "feedback_likert_scale") {
                return 'FeedbackLikert';
            }

            if (obj.acf_fc_layout === "feedback_multiple_choice") {
                return 'FeedbackMultipleChoice';
            }

            if (obj.acf_fc_layout === "feedback_text") {
                return 'FeedbackText';
            }

            return null;
        }
    }
}

export default resolvers