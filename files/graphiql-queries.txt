{
  posts {
    acf {
      main_screen {
        image
        blurb
      }
      content_types {
        acf_fc_layout
      }
    }
  }
}

/////////////////////////
{
  contentTypeByType(id: 6, type: "illustration") {
    acf_fc_layout
    title
  }
}

//////////////////////////
// fragments

//select one content type
{
  contentTypeByType(id: 6, type: "video") {
    acf_fc_layout
    ... on Video {
      title
      blurb
      main_screen_button_thumbnail
    }
  }
}

//show all content types
{
  contentTypes(id: 6) {
      acf_fc_layout
      	... on Video {
            title
            blurb
          	main_screen_button_thumbnail
        }
  			... on Illustration {
            title
            blurb
        }
    		... on FlipCards {
            title
            blurb
        }
   
  }
}

{
  postBySlug(slug: "city") {
      acf {
        card_title
        
        cards {
          acf_fc_layout
          ... on MultipleChoiceMultiple {
            quiz_description
            quiz {
              quiz_title
              quiz_options {
                quiz_input_type
                quiz_input_value
              }
            }
        	}
        }
        
      }
      	
  }
}