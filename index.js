import http from 'http'
import app from './server'

//set port
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

const port = normalizePort(process.env.port || 3000);

//create server
const httpServer = http.createServer(app)

let currentApp = app

httpServer.listen(port, () => {
    console.log(`Server is listening on port ${port}`)
})

if (module.hot) {
    module.hot.accept(['./server'], () => {
        httpServer.removeListener('request', currentApp)
        httpServer.on('request', app)
        currentApp = app
    })
}