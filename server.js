const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const logger = require('morgan')
const nunjucks = require('nunjucks')
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express')
const { makeExecutableSchema } = require('graphql-tools')
const compression = require('compression')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const config = require('./webpack.common.js')
const compiler = webpack(config)
const asyncHandler = require('express-async-handler')
const favicon = require('serve-favicon')

// Initialize the app
const app = express()
// view engine setup
app.set('views', path.join(__dirname, 'views'))

nunjucks.configure('./views', {
  express: app,
  autoescape: false,
  noCache: false,
  watch: true
});
app.set('view engine', 'njk')

//middleware
//json parsing
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//compress files
app.use(compression())
// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
app.use(webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath
}))
//favicon
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
//logging
app.use(logger('dev'))
//public path
app.use('/static', express.static(path.join(__dirname, 'public')))


// GraphQl stuff
// schema imports
import typeDefs from './api/typeDefinitions'
import resolvers from './api/resolvers'
// The schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers
})
// The GraphQL endpoint
app.use('/graphql', bodyParser.json(), graphqlExpress({ 
  schema,
  context: {},
  tracing: true,
  cacheControl: true 
}))
// GraphiQL, visual editor for queries
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))

// routes
import { getPosts, getPostBySlug, getPostTitles} from './api/graphqlRouter'

app.get('/', asyncHandler(async (req, res, next) => {
  const postTitles = await getPostTitles()
  const posts = await getPosts()
  
  res.render('index', {
    postList: postTitles.postTitle,
    posts: posts.posts
  })
}))


app.get('/post/:slug/', asyncHandler(async (req, res, next) => {
  const postTitles = await getPostTitles()
  const postBySlug = await getPostBySlug(req.params.slug)
  const posts = await getPosts()
  res.render('post', {
    post: postBySlug.postBySlug,
    postList: postTitles.postTitle,
    posts: posts.posts
  });
}))

export default app